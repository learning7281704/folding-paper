# Folding Paper

Explore my personal blog dedicated to the captivating world of origami (**oru** - to fold, **kami** - paper).
<br>
Discover a variety of detailed projects that showcase the beauty and intricacy of this timeless craft.
<br>
_Join me on a creative journey !_

# Project Setup

- Find documentations: [`documentations`](documentations/project.md)

## Typescript setup

1. Install typescript

```bash
npm i --save-dev typescript @types/node
```

2. Create typescript config (if not present already)

```bash
npx tsc --init
```

Sample tsconfig:

```json
{
  "include": ["src", "frontend"],
  "compilerOptions": {
    "outDir": "./build",
    "target": "es2016",
    "lib": ["es2016", "dom"],
    "strict": true,
    "esModuleInterop": true,
    "skipLibCheck": true
  }
}
```

3. Compile typescript

```bash
npm run watch-ts
```

## SCSS setup

Install `Live Sass Compiler` from vscode extensions

## Start

1. Install npm packages and start

- For developement

```bash
npm install
npm start
```

- For production

```bash
npm install --production
npm start
```

2. To run in dev mode

```bash
npm run dev
```

# Credits

- Icons: https://www.flaticon.com
- Fonts: https://www.fontspace.com
