var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
import admin from "firebase-admin";
import { initializeApp } from "firebase-admin/app";
import { getAuth } from "firebase-admin/auth";
import mongo_client from "./../services/db.service.js";
import dotenv from "dotenv";
import cookie from "cookie";
dotenv.config();
import serviceAccount from "../../firebase-keys.json" assert { type: "json" };
// const serviceAccount = JSON.parse(process.env.FIREBASE_JSON);
const firebase_app = initializeApp({
    credential: (_a = admin.credential) === null || _a === void 0 ? void 0 : _a.cert(serviceAccount),
    storageBucket: "folding-paper.appspot.com",
});
const createNewUser = (data) => __awaiter(void 0, void 0, void 0, function* () {
    data["created_at"] = new Date();
    data["liked_posts"] = [];
    data["liked_comments"] = [];
    data["bookmarked_posts"] = [];
    try {
        let db = mongo_client.db("folding-paper");
        let user_DB = yield db.collection("users").findOne({ uid: data["uid"] });
        if (user_DB != null)
            return;
        let res = yield db
            .collection("users")
            .findOneAndUpdate({ email: data["email"] }, { $set: Object.assign({}, data) }, { upsert: true });
        console.log(`user created: ${data["email"]}`);
    }
    catch (e) {
        console.log("user creation failed");
        console.log(e);
    }
});
export const fetchLoggedInUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    req.userData = undefined;
    if (!req.headers.cookie) {
        console.log("no cookie");
        next();
        return;
    }
    try {
        let cookieData = cookie.parse(req.headers.cookie);
        let idToken = cookieData["idToken"];
        // console.log(idToken);
        let decodedToken = yield getAuth().verifyIdToken(idToken);
        // console.log(decodedToken);
        let data = {};
        data.email = decodedToken.email;
        data.name = decodedToken.name;
        data.uid = decodedToken.uid;
        data.photoURL = decodedToken.picture;
        // console.log(data);
        req.userData = data;
        console.log(`user already logged in: ${data.email}`);
        yield createNewUser(data);
        next();
    }
    catch (error) {
        console.log(error);
        console.log("auth error");
        next();
    }
});
export const check403 = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    if (req.userData === undefined) {
        res.sendFile("403.html", { root: "./public" });
        return;
    }
    next();
});
