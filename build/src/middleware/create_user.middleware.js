var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import mongo_client from "./../services/db.service.js";
const updateUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    let data = req.userData;
    if (data === undefined) {
        next();
        return;
    }
    data["created_at"] = new Date();
    try {
        let db = mongo_client.db("folding-paper");
        let res = yield db
            .collection("users")
            .findOneAndUpdate({ email: data["email"] }, { $set: Object.assign({}, data) }, { upsert: true });
        console.log(`user updated: ${data["email"]}`);
    }
    catch (e) {
        console.log("user updated failed");
        console.log(e);
    }
    next();
});
export default updateUser;
