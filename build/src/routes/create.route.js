var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import express from "express";
import mongo_client from "./../services/db.service.js";
import path from "path";
import post_schema from "./../validation-schema/post.schema.js";
const __dirname = path.resolve();
// middleware
import { fetchLoggedInUser, } from "./../middleware/auth.middleware.js";
import { ObjectId } from "mongodb";
const create_router = express.Router({ mergeParams: true });
create_router.use("/", express.static(__dirname + "/public"));
create_router.get("/", fetchLoggedInUser, (req, res) => {
    res.sendFile("edit-blog.html", { root: "./public" });
});
create_router.post("/", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log(req.userData);
    if (req.userData === undefined) {
        res.status(401);
        res.json({ success: false, msg: "unauthorized accesss" });
        return;
    }
    let inp_data = req.body;
    // console.log(inp_data);
    let validation = post_schema.validate(inp_data);
    if (validation.error != null) {
        res.status(400);
        res.json({ success: false, msg: validation.error.details[0].message });
        return;
    }
    let data = Object.assign(Object.assign({}, inp_data), { created_by: req.userData["email"], author_uid: req.userData["uid"], created_at: new Date(), modified_at: new Date(), like_count: 0, comment_count: 0, is_archived: false });
    // console.log(data);
    try {
        let db = mongo_client.db("folding-paper");
        let res = yield db.collection("posts").insertOne(data);
        console.log(`Post created by: ${data["created_by"]}`);
    }
    catch (e) {
        console.log(e);
        res.status(500);
        res.json({ success: false, msg: "server error" });
        return;
    }
    res.json({ success: true });
}));
create_router.post("/edit", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log(req.userData);
    if (req.userData === undefined) {
        res.status(401);
        res.json({ success: false, msg: "unauthorized accesss" });
        return;
    }
    let inp_data = req.body;
    // console.log(inp_data);
    let validation = post_schema.validate(inp_data);
    if (validation.error != null) {
        res.status(400);
        res.json({ success: false, msg: validation.error.details[0].message });
        return;
    }
    // check if post exists
    let post_res = undefined;
    try {
        let db = mongo_client.db("folding-paper");
        post_res = yield db
            .collection("posts")
            .findOne({ _id: new ObjectId(inp_data["_id"]) });
        if (post_res == null) {
            res.status(404);
            res.json({ success: false, msg: "post not found" });
            return;
        }
    }
    catch (error) {
        console.log(error);
        res.status(500);
        res.json({ success: false, msg: "server error" });
        return;
    }
    if (post_res["author_uid"] != req.userData.uid) {
        res.status(401);
        res.json({ success: false, msg: "unauthorized accesss" });
        return;
    }
    let data = Object.assign(Object.assign({}, inp_data), { modified_at: new Date() });
    delete data["_id"];
    // console.log(data);
    try {
        let db = mongo_client.db("folding-paper");
        let res = yield db
            .collection("posts")
            .findOneAndUpdate({ _id: new ObjectId(inp_data["_id"]) }, { $set: Object.assign({}, data) });
        // console.log(res);
        console.log(`Post edited by: ${req.userData.email}`);
    }
    catch (e) {
        console.log(e);
        res.status(500);
        res.json({ success: false, msg: "server error" });
        return;
    }
    res.json({ success: true });
}));
export default create_router;
