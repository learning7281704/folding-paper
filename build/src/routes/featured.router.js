var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import express from "express";
import mongo_client from "./../services/db.service.js";
const featured_router = express.Router({ mergeParams: true });
featured_router.get("/new", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let res_data = [];
    try {
        let db = mongo_client.db("folding-paper");
        let res = db.collection("posts").find().sort({ modified_at: -1 });
        res_data = yield res.toArray();
        // console.log(res);
    }
    catch (e) {
        console.log(e);
        res.status(500);
        res.json({ success: false, msg: "server error" });
        return;
    }
    res.json({ success: true, data: res_data });
}));
export default featured_router;
