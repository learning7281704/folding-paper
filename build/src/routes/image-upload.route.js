var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import express from "express";
import { v4 as uuidv4 } from "uuid";
import mongo_client from "./../services/db.service.js";
import { uploadImage } from "./../services/firebase.storage.service.js";
// middleware
import { fetchLoggedInUser } from "./../middleware/auth.middleware.js";
import { image_file_check } from "./../middleware/image.middleware.js";
const image_upload_router = express.Router({ mergeParams: true });
const store_image_link = (user_email, image_link) => __awaiter(void 0, void 0, void 0, function* () {
    let data = {
        user_email,
        upload_link: image_link,
        created_at: new Date(),
    };
    try {
        let db = mongo_client.db(process.env.DBNAME);
        yield db.collection("uploads").insertOne(data);
        console.log(`image link uploaded to db by: ${user_email}`);
    }
    catch (e) {
        console.log("error: in image upload");
        console.log(e);
    }
});
image_upload_router.use("/", [image_file_check, fetchLoggedInUser], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    if ((req === null || req === void 0 ? void 0 : req.userData) === undefined) {
        res.status(401);
        res.json({ success: false, msg: "unauthorised access" });
        return;
    }
    let file = (_a = req === null || req === void 0 ? void 0 : req.files) === null || _a === void 0 ? void 0 : _a.file_input;
    let file_name = file["name"];
    let file_extension = file_name.split(".");
    file_extension = file_extension[file_extension.length - 1];
    // console.log(file_extension);
    let url = yield uploadImage(`${uuidv4()}.${file_extension}`, file.data, file_extension);
    //   console.log(url);
    yield store_image_link(req.userData.email, url);
    res.json({ success: true, url });
}));
export default image_upload_router;
