var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import express from "express";
import mongo_client from "./../services/db.service.js";
import { ObjectId } from "mongodb";
import path from "path";
const __dirname = path.resolve();
// console.log(__dirname);
// middleware
import { fetchLoggedInUser } from "../middleware/auth.middleware.js";
const post_router = express.Router({ mergeParams: true });
post_router.use("/", express.static(__dirname + "/public"));
post_router.get("/", (req, res) => {
    res.redirect("/");
});
post_router.get("/:postID", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    let postID = (_a = req.params) === null || _a === void 0 ? void 0 : _a.postID;
    try {
        let db = mongo_client.db(process.env.DBNAME);
        let res = yield db
            .collection("posts")
            .findOne({ _id: new ObjectId(postID) });
        // console.log(res);
        if (res === null)
            throw new Error("post not found");
    }
    catch (error) {
        console.log(error);
        res.status(404);
        res.sendFile("404.html", { root: "./public" });
        return;
    }
    res.sendFile("blog-post.html", { root: "./public" });
}));
post_router.get("/:postID/data", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _b, _c;
    let postID = (_b = req.params) === null || _b === void 0 ? void 0 : _b.postID;
    let res_data;
    try {
        let db = mongo_client.db(process.env.DBNAME);
        res_data = yield db
            .collection("posts")
            .findOne({ _id: new ObjectId(postID) });
        // console.log(res);
    }
    catch (error) {
        console.log(error);
        res.status(404);
        res.json({ success: false });
        return;
    }
    if (res_data["is_archived"] == true &&
        ((_c = req === null || req === void 0 ? void 0 : req.userData) === null || _c === void 0 ? void 0 : _c.uid) != res_data["author_uid"]) {
        res_data["cover_image_link"] = "";
        res_data["title"] = "Archived";
        res_data["tags"] = [];
        res_data["content"] = "This post has been archived by the user.";
    }
    res.json({ sucess: true, data: res_data });
}));
post_router.post("/:postID/like", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _d;
    let postID = (_d = req.params) === null || _d === void 0 ? void 0 : _d.postID;
    if (req.userData === undefined) {
        res.json({ success: false, msg: "user not logged in" });
        return;
    }
    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
        session.startTransaction();
        let db = mongo_client.db(process.env.DBNAME);
        // check if user has already liked  the post
        let user_data = yield db
            .collection("users")
            .findOne({ uid: req.userData.uid });
        if (Object.keys(user_data).indexOf("liked_posts") == -1) {
            user_data["liked_posts"] = [];
        }
        if ((user_data === null || user_data === void 0 ? void 0 : user_data.liked_posts.indexOf(postID)) != -1) {
            error_msg = "Already liked";
            throw new Error("already liked");
        }
        // update like count
        yield db
            .collection("posts")
            .updateOne({ _id: new ObjectId(postID) }, { $inc: { like_count: 1 } });
        // add postID to user doc
        yield db
            .collection("users")
            .updateOne({ uid: req.userData.uid }, { $push: { liked_posts: postID } });
        yield session.commitTransaction();
        console.log("Transaction committed.");
    }
    catch (error) {
        yield session.abortTransaction();
        console.log(error);
        res.status(500);
        res.json({ success: false, msg: error_msg });
        return;
    }
    finally {
        yield session.endSession();
    }
    res.json({ sucess: true });
}));
post_router.post("/:postID/unlike", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _e;
    let postID = (_e = req.params) === null || _e === void 0 ? void 0 : _e.postID;
    if (req.userData === undefined) {
        res.json({ success: false, msg: "user not logged in" });
        return;
    }
    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
        session.startTransaction();
        let db = mongo_client.db(process.env.DBNAME);
        // check if user has already liked  the post
        let user_data = yield db
            .collection("users")
            .findOne({ uid: req.userData.uid });
        if (Object.keys(user_data).indexOf("liked_posts") == -1) {
            user_data["liked_posts"] = [];
        }
        if ((user_data === null || user_data === void 0 ? void 0 : user_data.liked_posts.indexOf(postID)) == -1) {
            error_msg = "not liked yet";
            throw new Error("already liked yet");
        }
        // update like count
        yield db
            .collection("posts")
            .updateOne({ _id: new ObjectId(postID) }, { $inc: { like_count: -1 } });
        // add postID to user doc
        yield db
            .collection("users")
            .updateOne({ uid: req.userData.uid }, { $pull: { liked_posts: postID } });
        yield session.commitTransaction();
        console.log("Transaction committed.");
    }
    catch (error) {
        yield session.abortTransaction();
        console.log(error);
        res.status(500);
        res.json({ success: false, msg: error_msg });
        return;
    }
    finally {
        yield session.endSession();
    }
    res.json({ sucess: true });
}));
post_router.post("/:postID/add-to-bookmark", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _f;
    let postID = (_f = req.params) === null || _f === void 0 ? void 0 : _f.postID;
    if (req.userData === undefined) {
        res.json({ success: false, msg: "user not logged in" });
        return;
    }
    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
        session.startTransaction();
        let db = mongo_client.db(process.env.DBNAME);
        // check if user has already liked  the post
        let user_data = yield db
            .collection("users")
            .findOne({ uid: req.userData.uid });
        if (Object.keys(user_data).indexOf("bookmarked_posts") == -1) {
            user_data["bookmarked_posts"] = [];
        }
        if ((user_data === null || user_data === void 0 ? void 0 : user_data.bookmarked_posts.indexOf(postID)) != -1) {
            error_msg = "Already bookmarked";
            throw new Error("Already bookmarked");
        }
        // add postID to user bookmark doc
        yield db
            .collection("users")
            .updateOne({ uid: req.userData.uid }, { $push: { bookmarked_posts: postID } });
        yield session.commitTransaction();
        console.log("Transaction committed.");
    }
    catch (error) {
        yield session.abortTransaction();
        console.log(error);
        res.status(500);
        res.json({ success: false, msg: error_msg });
        return;
    }
    finally {
        yield session.endSession();
    }
    res.json({ sucess: true });
}));
post_router.post("/:postID/remove-from-bookmark", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _g;
    let postID = (_g = req.params) === null || _g === void 0 ? void 0 : _g.postID;
    if (req.userData === undefined) {
        res.json({ success: false, msg: "user not logged in" });
        return;
    }
    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
        session.startTransaction();
        let db = mongo_client.db(process.env.DBNAME);
        // check if user has already liked  the post
        let user_data = yield db
            .collection("users")
            .findOne({ uid: req.userData.uid });
        if (Object.keys(user_data).indexOf("bookmarked_posts") == -1) {
            user_data["bookmarked_posts"] = [];
        }
        if ((user_data === null || user_data === void 0 ? void 0 : user_data.bookmarked_posts.indexOf(postID)) == -1) {
            error_msg = "Not in bookmark yet";
            throw new Error("Not in bookmark yet");
        }
        // add postID to user bookmark doc
        yield db
            .collection("users")
            .updateOne({ uid: req.userData.uid }, { $pull: { bookmarked_posts: postID } });
        yield session.commitTransaction();
        console.log("Transaction committed.");
    }
    catch (error) {
        yield session.abortTransaction();
        console.log(error);
        res.status(500);
        res.json({ success: false, msg: error_msg });
        return;
    }
    finally {
        yield session.endSession();
    }
    res.json({ sucess: true });
}));
post_router.post("/:postID/add-to-archive", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _h;
    let postID = (_h = req.params) === null || _h === void 0 ? void 0 : _h.postID;
    if (req.userData === undefined) {
        res.json({ success: false, msg: "user not logged in" });
        return;
    }
    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
        session.startTransaction();
        let db = mongo_client.db(process.env.DBNAME);
        // check if user has already liked  the post
        let post_data = yield db
            .collection("posts")
            .findOne({ _id: new ObjectId(postID) });
        if (post_data["author_uid"] != req.userData.uid) {
            error_msg = "Unauthorised access";
            throw new Error("Unauthorised access");
        }
        if (post_data === null || post_data === void 0 ? void 0 : post_data.is_archived) {
            error_msg = "Already archived";
            throw new Error("Already archived");
        }
        // add postID to user bookmark doc
        yield db
            .collection("posts")
            .updateOne({ _id: new ObjectId(postID) }, { $set: { is_archived: true } });
        yield session.commitTransaction();
        console.log("Transaction committed.");
    }
    catch (error) {
        yield session.abortTransaction();
        console.log(error);
        res.status(500);
        res.json({ success: false, msg: error_msg });
        return;
    }
    finally {
        yield session.endSession();
    }
    res.json({ sucess: true });
}));
post_router.post("/:postID/remove-from-archive", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _j;
    let postID = (_j = req.params) === null || _j === void 0 ? void 0 : _j.postID;
    if (req.userData === undefined) {
        res.json({ success: false, msg: "user not logged in" });
        return;
    }
    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
        session.startTransaction();
        let db = mongo_client.db(process.env.DBNAME);
        // check if user has already liked  the post
        let post_data = yield db
            .collection("posts")
            .findOne({ _id: new ObjectId(postID) });
        if (post_data["author_uid"] != req.userData.uid) {
            error_msg = "Unauthorised access";
            throw new Error("Unauthorised access");
        }
        if ((post_data === null || post_data === void 0 ? void 0 : post_data.is_archived) == false) {
            error_msg = "Already not in archived";
            throw new Error("Already not in archived");
        }
        // add postID to user bookmark doc
        yield db
            .collection("posts")
            .updateOne({ _id: new ObjectId(postID) }, { $set: { is_archived: false } });
        yield session.commitTransaction();
        console.log("Transaction committed.");
    }
    catch (error) {
        yield session.abortTransaction();
        console.log(error);
        res.status(500);
        res.json({ success: false, msg: error_msg });
        return;
    }
    finally {
        yield session.endSession();
    }
    res.json({ sucess: true });
}));
// all comments
post_router.get("/:postID/comments", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _k;
    let postID = (_k = req.params) === null || _k === void 0 ? void 0 : _k.postID;
    let comm_res = [];
    try {
        let db = mongo_client.db(process.env.DBNAME);
        comm_res = db
            .collection("comments")
            .find({ post_id: postID, parent_comment_id: "" })
            .sort({ modified_at: -1 });
        // console.log(comm_res);
        if (comm_res === null)
            throw new Error("post not found");
        comm_res = yield comm_res.toArray();
    }
    catch (error) {
        console.log(error);
        res.status(500);
        res.json({ success: true, data: [], msg: "server error" });
        return;
    }
    res.json({ success: true, data: comm_res });
}));
export default post_router;
