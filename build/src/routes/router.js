import express from "express";
// routers
import post_router from "./posts.route.js";
import create_router from "./create.route.js";
import featured_router from "./featured.router.js";
import user_router from "./user.route.js";
import image_upload_router from "./image-upload.route.js";
import comment_router from "./comment.route.js";
import search_router from "./search.route.js";
const router = express.Router();
router.get("/", (req, res) => {
    res.sendFile("landing.html", { root: "./public" });
});
// blog post route
router.use("/post", post_router);
// comments
router.use("/comment", comment_router);
// create blog
router.use("/create", create_router);
// get featured posts
router.use("/featured-posts", featured_router);
// get search result for posts
router.use("/search", search_router);
// // testing admin features
// router.get(
//   "/admin",
//   [fetchLoggedInUser, check403, updateUser],
//   (req: any, res: any) => {
//     res.json({ msg: "yaya" });
//   }
// );
// upload image
router.use("/image-upload", image_upload_router);
// user profile
router.use("/user", user_router);
router.use((req, res) => {
    res.status(404).sendFile("404.html", { root: "./public" });
});
export default router;
