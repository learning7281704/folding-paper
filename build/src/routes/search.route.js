var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import express from "express";
import mongo_client from "./../services/db.service.js";
const search_router = express.Router({ mergeParams: true });
search_router.post("/posts", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    let title = (_a = req.body) === null || _a === void 0 ? void 0 : _a.title;
    // console.log(title);
    if (!title || title.length < 3) {
        res.status(403);
        res.json({ status: false, msg: "invalid input!", data: [] });
        return;
    }
    let error_msg = "server error";
    let posts = [];
    try {
        let db = mongo_client.db(process.env.DBNAME);
        const agg = [
            {
                $search: {
                    index: 'search_posts',
                    text: {
                        query: title,
                        path: {
                            wildcard: "*"
                        },
                        fuzzy: {
                            maxEdits: 2,
                            prefixLength: 0,
                            maxExpansions: 50
                        }
                    }
                }
            },
            { $sort: { modified_at: -1 } },
            { $limit: 20 },
        ];
        // run pipeline
        let result = db.collection("posts").aggregate(agg);
        posts = yield result.toArray();
        // console.log(posts);
    }
    catch (error) {
        console.log(error);
        res.status(500);
        res.json({ success: false, msg: error_msg });
        return;
    }
    res.json({ sucess: true, data: posts });
}));
search_router.post("/autocomplete", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    let title = (_b = req.body) === null || _b === void 0 ? void 0 : _b.title;
    if (!title || title.length <= 3) {
        res.status(403);
        res.json({ status: false, msg: "invalid input!" });
        return;
    }
    let error_msg = "server error";
    try {
        let db = mongo_client.db(process.env.DBNAME);
        const agg = [
            {
                $search: {
                    index: "search_posts",
                    autocomplete: { query: title, path: "title" },
                },
            },
            { $limit: 20 },
            { $project: { _id: 0, title: 1 } },
        ];
        // run pipeline
        let result = db.collection("posts").aggregate(agg);
        let posts = yield result.toArray();
        console.log(posts);
    }
    catch (error) {
        console.log(error);
        res.status(500);
        res.json({ success: false, msg: error_msg });
        return;
    }
    res.json({ sucess: true });
}));
export default search_router;
