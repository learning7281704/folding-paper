var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import express from "express";
import mongo_client from "./../services/db.service.js";
import { ObjectId } from "mongodb";
import path from "path";
// middleware
import { fetchLoggedInUser, } from "./../middleware/auth.middleware.js";
const __dirname = path.resolve();
// console.log(__dirname);
const user_router = express.Router({ mergeParams: true });
user_router.use("/", express.static(__dirname + "/public"));
user_router.get("/", (req, res) => {
    res.redirect("/");
});
user_router.get("/:userUID", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    let userUID = (_a = req.params) === null || _a === void 0 ? void 0 : _a.userUID;
    try {
        let db = mongo_client.db(process.env.DBNAME);
        let res = yield db.collection("users").findOne({ uid: userUID });
        // console.log(res);
        if (res === null)
            throw new Error("user not found");
    }
    catch (error) {
        console.log(error);
        res.status(404);
        res.sendFile("404.html", { root: "./public" });
        return;
    }
    res.sendFile("profile-page.html", { root: "./public" });
}));
user_router.get("/:userUID/data", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    let userUID = (_b = req.params) === null || _b === void 0 ? void 0 : _b.userUID;
    let res_data;
    try {
        let db = mongo_client.db(process.env.DBNAME);
        res_data = yield db.collection("users").findOne({ uid: userUID });
        // console.log(res);
    }
    catch (error) {
        console.log(error);
        res.status(404);
        res.json({ success: false });
        return;
    }
    res.json({ sucess: true, data: res_data });
}));
user_router.get("/:userUID/all-posts", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    let userUID = (_c = req.params) === null || _c === void 0 ? void 0 : _c.userUID;
    let res_data;
    try {
        let db = mongo_client.db(process.env.DBNAME);
        res_data = db.collection("posts").find({ author_uid: userUID });
        res_data = yield res_data.toArray();
        // console.log(res);
    }
    catch (error) {
        console.log(error);
        res.status(404);
        res.json({ success: false });
        return;
    }
    res.json({ sucess: true, data: res_data });
}));
user_router.get("/:userUID/all-comments", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _d;
    let userUID = (_d = req.params) === null || _d === void 0 ? void 0 : _d.userUID;
    let res_data;
    try {
        let db = mongo_client.db(process.env.DBNAME);
        res_data = db.collection("comments").find({ author_uid: userUID });
        res_data = yield res_data.toArray();
        // console.log(res);
    }
    catch (error) {
        console.log(error);
        res.status(404);
        res.json({ success: false });
        return;
    }
    res.json({ sucess: true, data: res_data });
}));
user_router.get("/:userUID/liked-posts", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _e;
    let userUID = (_e = req.params) === null || _e === void 0 ? void 0 : _e.userUID;
    if (req.userData === undefined || userUID != req.userData.uid) {
        res.status(403);
        res.json({
            success: false,
            data: [],
            msg: "unauthorised access requested !",
        });
        return;
    }
    let res_data;
    try {
        let db = mongo_client.db(process.env.DBNAME);
        let user_data = yield db
            .collection("users")
            .findOne({ uid: userUID });
        if (Object.keys(user_data).indexOf("liked_posts") == -1) {
            res.json({ success: true, data: [] });
            return;
        }
        let ids = user_data["liked_posts"];
        ids = ids.map((e) => {
            return new ObjectId(e);
        });
        res_data = db.collection("posts").find({ _id: { $in: ids } });
        res_data = yield res_data.toArray();
        // console.log(res);
    }
    catch (error) {
        console.log(error);
        res.status(404);
        res.json({ success: false });
        return;
    }
    res.json({ sucess: true, data: res_data });
}));
user_router.get("/:userUID/liked-comments", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _f;
    let userUID = (_f = req.params) === null || _f === void 0 ? void 0 : _f.userUID;
    if (req.userData === undefined || userUID != req.userData.uid) {
        res.status(403);
        res.json({
            success: false,
            data: [],
            msg: "unauthorised access requested !",
        });
        return;
    }
    let res_data;
    try {
        let db = mongo_client.db(process.env.DBNAME);
        let user_data = yield db
            .collection("users")
            .findOne({ uid: userUID });
        if (Object.keys(user_data).indexOf("liked_comments") == -1) {
            res.json({ success: true, data: [] });
            return;
        }
        let ids = user_data["liked_comments"];
        ids = ids.map((e) => {
            return new ObjectId(e);
        });
        res_data = db.collection("comments").find({ _id: { $in: ids } });
        res_data = yield res_data.toArray();
        // console.log(res);
    }
    catch (error) {
        console.log(error);
        res.status(404);
        res.json({ success: false });
        return;
    }
    res.json({ sucess: true, data: res_data });
}));
user_router.get("/:userUID/archived-posts", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _g;
    let userUID = (_g = req.params) === null || _g === void 0 ? void 0 : _g.userUID;
    if (req.userData === undefined || userUID != req.userData.uid) {
        res.status(403);
        res.json({
            success: false,
            data: [],
            msg: "unauthorised access requested !",
        });
        return;
    }
    let res_data;
    try {
        let db = mongo_client.db(process.env.DBNAME);
        let user_data = yield db
            .collection("users")
            .findOne({ uid: userUID });
        res_data = db
            .collection("posts")
            .find({ author_uid: req.userData.uid, is_archived: true });
        res_data = yield res_data.toArray();
        // console.log(res);
    }
    catch (error) {
        console.log(error);
        res.status(404);
        res.json({ success: false });
        return;
    }
    res.json({ sucess: true, data: res_data });
}));
user_router.get("/:userUID/bookmarked-posts", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _h;
    let userUID = (_h = req.params) === null || _h === void 0 ? void 0 : _h.userUID;
    if (req.userData === undefined || userUID != req.userData.uid) {
        res.status(403);
        res.json({
            success: false,
            data: [],
            msg: "unauthorised access requested !",
        });
        return;
    }
    let res_data;
    try {
        let db = mongo_client.db(process.env.DBNAME);
        let user_data = yield db
            .collection("users")
            .findOne({ uid: userUID });
        if (Object.keys(user_data).indexOf("bookmarked_posts") == -1) {
            res.json({ success: true, data: [] });
            return;
        }
        let ids = user_data["bookmarked_posts"];
        ids = ids.map((e) => {
            return new ObjectId(e);
        });
        res_data = db.collection("posts").find({ _id: { $in: ids } });
        res_data = yield res_data.toArray();
        // console.log(res);
    }
    catch (error) {
        console.log(error);
        res.status(404);
        res.json({ success: false });
        return;
    }
    res.json({ sucess: true, data: res_data });
}));
user_router.get("/:userUID/uploads", fetchLoggedInUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _j;
    let userUID = (_j = req.params) === null || _j === void 0 ? void 0 : _j.userUID;
    if (req.userData === undefined || userUID != req.userData.uid) {
        res.status(403);
        res.json({
            success: false,
            data: [],
            msg: "unauthorised access requested !",
        });
        return;
    }
    let data = [];
    try {
        let db = mongo_client.db(process.env.DBNAME);
        data = db
            .collection("uploads")
            .find({ user_email: req.userData.email })
            .sort({ created_at: -1 });
        data = yield data.toArray();
    }
    catch (e) {
        console.log(e);
        res.status(500);
        res.json({ success: false, data });
        return;
    }
    res.json({ success: true, data });
}));
// all comments
user_router.get("/:userUID/comments", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _k;
    let userUID = (_k = req.params) === null || _k === void 0 ? void 0 : _k.userUID;
    let comm_res = [];
    try {
        let db = mongo_client.db(process.env.DBNAME);
        comm_res = db
            .collection("comments")
            .find({ author_uid: userUID })
            .sort({ modified_at: -1 });
        // console.log(comm_res);
        if (comm_res === null)
            throw new Error("post not found");
        comm_res = yield comm_res.toArray();
    }
    catch (error) {
        console.log(error);
        res.status(500);
        res.json({ success: true, data: [], msg: "server error" });
        return;
    }
    res.json({ success: true, data: comm_res });
}));
export default user_router;
