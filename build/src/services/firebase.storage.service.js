var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import admin from "firebase-admin";
const bucket = admin.storage().bucket();
export const uploadImage = (destination, image, file_extension) => __awaiter(void 0, void 0, void 0, function* () {
    const file = bucket.file(destination);
    yield file.save(image, { contentType: file_extension });
    let file_name = `https://firebasestorage.googleapis.com/v0/b/folding-paper.appspot.com/o/${destination}?alt=media`;
    return file_name;
});
