import Joi from "joi";
const comment_schema = Joi.object().keys({
    text: Joi.string().min(10).required(),
    post_id: Joi.string().length(24).required(),
    parent_comment_id: Joi.string().allow(null, "").max(24).required(),
    parent_author_email: Joi.string()
        .allow(null, "")
        .email({ tlds: { allow: false } })
        .required(),
});
export default comment_schema;
