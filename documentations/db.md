# Database schema documentation

Detials regarding the mongodb collections and it's utility

## posts

posts collections to hold all blog-posts

|   \_id   | title  | cover_image_link | content |   tags   | created_by | created_at | modified_at |
| :------: | :----: | :--------------: | :-----: | :------: | :--------: | :--------: | :---------: |
| ObjectID | string |      string      | string  | string[] |  ObjectID  |    Date    |    Date     |
