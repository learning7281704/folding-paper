# Project overview

A user-friendly blogging platform hosted on Render, featuring markdown-based post creation, image uploads via Firebase, Google Sign-In for comments, user profile pages, and seamless search capabilities using MongoDB Atlas Search.

## Project requirements

### 1. Landing page

- Feature a visually appealing banner.
- Display curated featured blogs.
- Highlight the most recent or popular blog posts.

### 2. Blogs -> posts

- Support markdown elements in blog posts.
- Allow image upload via link or file using Firebase.

### 3. comment section

- Enable threaded discussions through recursive comments.
- Implement Google Sign-In for user authentication in the comment section.

### 4. user profile page

- Provide a dedicated profile page for each logged-in user.
- Allow users to create, edit, and manage their own blog posts.
- Allow users to like, comment, and bookmark posts

### 5. Search

- Integrate MongoDB Atlas Search for efficient and user-friendly search capabilities.
  -Enable users to search for posts and comments seamlessly.

## Features

### By page

|       Page        |         Public          | Authenticated user (loggedin) |                                      Authorised user (author)                                      |
| :---------------: | :---------------------: | :---------------------------: | :------------------------------------------------------------------------------------------------: |
|   Landing page    |          Feed           |               -               |                                                 -                                                  |
|     Blog page     |  Content and comments   |  like, comment, and bookmark  |                                         Edit, and Archive                                          |
| User profile page | All posts, and comments |    All posts, and comments    | All posts,<br> All comments,<br> liked posts and comments,<br> bookmarks,<br> uploads and archives |
|     Edit page     |           NA            |     create and edit posts     |                                                 -                                                  |

### By component/feature

|  Component/Feature  |           Public           | Authenticated user (loggedin) | Authorised user (author) |
| :-----------------: | :------------------------: | :---------------------------: | :----------------------: |
|       Nav bar       | branding,login, and search |   create, and profile navs    |            -             |
| Image upload widget |             NA             |      Upload and get link      |    Edit, and Archive     |
|     Search bar      |  search post and comments  |               -               |            -             |

## Tech stack

### Frontend

- HTML
- CSS/SCSS
- Javascript/Typescript

### Backend

- NodeJS
- ExpressJS

### Database

- MongoDB

### Cloud & hosting

- GCP or firebase (for google login)
- gitlab + onrender (for hosting)
