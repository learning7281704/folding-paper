import express from "express";
import fileUpload from "express-fileupload";
import cors from "cors";
import dotenv from "dotenv";
dotenv.config();

import path from "path";
const __dirname = path.resolve();

import router from "./src/routes/router.js";

const app = express();

// parser
app.use(express.json());
app.use(fileUpload());
app.use(cors());
app.use(express.static("public"));

app.use(router);

// listen for requests :)
// port infos
const port = process.env.PORT || 8000;
app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
