import { initializeApp } from "https://www.gstatic.com/firebasejs/9.19.1/firebase-app.js";
import {
  getAuth,
  onAuthStateChanged,
  signInWithPopup,
  signOut,
  GoogleAuthProvider,
} from "https://www.gstatic.com/firebasejs/9.19.1/firebase-auth.js";

import {
  show_loggedin_components,
  hide_loggedin_components,
  toggle_profile_nav,
} from "./_navbar.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAgYPDM1RGqoeYsHb6Q5Xsul59eujcgyPY",
  authDomain: "folding-paper.firebaseapp.com",
  projectId: "folding-paper",
  storageBucket: "folding-paper.appspot.com",
  messagingSenderId: "1006050672188",
  appId: "1:1006050672188:web:2201f0698c131545f54d39",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth();
let appUser = null;

export const auth_init = () => {
  document
    .getElementById("profile-image")
    .addEventListener("click", toggle_profile_nav);
  document
    .getElementById("close-profile")
    .addEventListener("click", toggle_profile_nav);

  document.getElementById("g-login").addEventListener("click", async () => {
    await loginUser();
  });
  document.getElementById("g-logout").addEventListener("click", () => {
    logoutUser();
  });

  return new Promise((resolve, reject) => {
    get_firebase_user(resolve, reject);
  });
};

const get_firebase_user = (resolve, reject) => {
  onAuthStateChanged(auth, (user) => {
    if (user) {
      // console.log(user);

      appUser = {};

      appUser.name = user.displayName;
      appUser.photoURL = user.photoURL;
      appUser.uid = user.uid;
      appUser.email = user.email;
      appUser.idToken = user.accessToken;

      delete_cookie("idToken");
      document.cookie = `idToken=${appUser.idToken};path=/`;

      // console.log(appUser);
      console.log(`Logged in user: ${appUser.name}`);

      try {
        show_loggedin_components(appUser);
      } catch (error) {
        console.log("no nav here");
      }
    } else {
      delete_cookie("idToken");
      appUser = null;
      console.log("user not available");

      try {
        hide_loggedin_components();
      } catch (error) {
        console.log("no nav here");
      }
    }
    resolve(appUser);
  });
};

const loginUser = async () => {
  try {
    let provider = new GoogleAuthProvider();

    let userCredential = await signInWithPopup(auth, provider);

    const token = userCredential.accessToken;

    let user = userCredential.user;
    // console.log(user);

    let idToken = user["accessToken"];
    document.cookie = `idToken=${appUser.idToken};path=/`;

    console.log("user logged in");
  } catch (e) {
    delete_cookie("idToken");
    if (appUser != null) {
      console.log("already logged in");
      return;
    }

    console.log(e);
    console.log("login failed");
  }
};

const logoutUser = () => {
  signOut(auth)
    .then(() => {
      delete_cookie("idToken");
      appUser = null;
      console.log("logged out");

      toggle_profile_nav();
      hide_loggedin_components();

      window.location = "/";
    })
    .catch((error) => {
      console.log("error in log out");
      console.log(error);
    });
};
const delete_cookie = function (name) {
  document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
};
