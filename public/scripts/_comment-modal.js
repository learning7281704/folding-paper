import { fetch_req } from "../scripts/_fetch.js";
import {
  add_comment_to_div,
  show_recursive_comments,
  hide_recursive_comments,
} from "./_comment-section.js";

let converter = new showdown.Converter({
  tables: true,
  simpleLineBreaks: true,
  setFlavor: "github",
});

let parent_comment_id = "",
  parent_email = "",
  is_edit = false;

export const open_comment_modal = () => {
  document.getElementsByTagName("section")[0].classList.add("blur");
  document
    .getElementsByClassName("comment-modal")[0]
    .classList.remove("hide-modal");
};
const close_comment_modal = () => {
  is_edit = false;
  document.getElementsByTagName("section")[0].classList.remove("blur");

  document
    .getElementsByClassName("comment-modal")[0]
    .classList.add("hide-modal");
};

const show_btn_event = (e) => {
  let elem = e.currentTarget;
  let is_open = elem.getAttribute("is_open");
  let comment_id = elem.getAttribute("comment-id");

  if (is_open == "false") {
    elem.getElementsByTagName("button")[0].classList.remove("hide");
    elem.getElementsByTagName("button")[1].classList.add("hide");
    elem.setAttribute("is_open", "true");
    show_recursive_comments(comment_id);
    return;
  }

  elem.getElementsByTagName("button")[1].classList.remove("hide");
  elem.getElementsByTagName("button")[0].classList.add("hide");
  elem.setAttribute("is_open", "false");
  hide_recursive_comments(comment_id);
};

const add_event_to_show_btn = () => {
  let divs = document.getElementsByClassName("show-more-comments");
  for (let i = 0; i < divs.length; ++i) {
    divs[i].removeEventListener("click", show_btn_event);
  }
  for (let i = 0; i < divs.length; ++i) {
    divs[i].addEventListener("click", show_btn_event);
  }
};

const reply_btn_event = (e) => {
  let elem = e.currentTarget;
  parent_comment_id = elem.getAttribute("parent-comment-ID");
  parent_email = elem.getAttribute("parent-comment-author");

  let reply_to_div = document.getElementById("comment-reply-to");
  if (parent_comment_id == "") {
    reply_to_div.innerText = "Comment on post:";
  } else {
    reply_to_div.innerText = `reply to: ${parent_email}`;
  }

  open_comment_modal();
};

export const add_event_to_reply_btns = () => {
  let reply_btns = document.getElementsByClassName("comment-reply");
  for (let i = 0; i < reply_btns.length; ++i) {
    reply_btns[i].removeEventListener("click", reply_btn_event);
  }
  for (let i = 0; i < reply_btns.length; ++i) {
    reply_btns[i].addEventListener("click", reply_btn_event);
  }

  add_event_to_show_btn();
};

const add_comment_to_edit = async (e) => {
  let elem = e.currentTarget;
  let comment_id = elem.getAttribute("comment-ID");
  parent_comment_id = comment_id;

  let comm_res = await fetch(`/comment/${comment_id}`);
  comm_res = await comm_res.json();
  comm_res = comm_res["data"];

  document.getElementById(
    "comment-reply-to"
  ).innerText = `Editing comment: ${comment_id}`;
  document.getElementById("comment-input").innerHTML = comm_res["text"];
  is_edit = true;
  open_comment_modal();
};

export const add_event_to_edit_comment_div = () => {
  let comment_edit_btns = document.getElementsByClassName("comment-edit");
  for (let i = 0; i < comment_edit_btns.length; ++i) {
    comment_edit_btns[i].removeEventListener("click", add_comment_to_edit);
  }
  for (let i = 0; i < comment_edit_btns.length; ++i) {
    comment_edit_btns[i].addEventListener("click", add_comment_to_edit);
  }
};

const edit_comment = () => {
  document.getElementById("comment-input").classList.remove("hide-modal");
  document.getElementById("comment-preview-output").classList.add("hide-modal");
};
const preview_comment = () => {
  document.getElementById("comment-input").classList.add("hide-modal");
  document
    .getElementById("comment-preview-output")
    .classList.remove("hide-modal");

  let input_text = document.getElementById("comment-input").value;
  let content = converter.makeHtml(input_text);

  let content_div = document.getElementById("comment-preview-output");
  content = filterXSS(content);
  content_div.innerHTML = content;
};
const submit_comment = async () => {
  let comment_inp = document.getElementById("comment-input").value;

  let data = {
    text: comment_inp,
    post_id: window.postID,
    parent_comment_id,
    parent_author_email: parent_email,
  };
  console.log(data);

  if (!data.post_id) {
    alert("Post not found");
    return;
  }

  let res;
  if (is_edit == true) {
    res = await fetch_req("/comment/edit", "POST", data);
    res = await res.json();
  } else {
    res = await fetch_req("/comment", "POST", data);
    res = await res.json();
  }

  // console.log(res);

  if (res["success"] == false) {
    alert(res["msg"]);
    return;
  }

  if (is_edit == true) {
    let input_text = comment_inp;
    let content = converter.makeHtml(input_text);

    let elem = document.getElementById(parent_comment_id);

    elem.parentElement.getElementsByClassName("comment-text")[0].innerHTML =
      content;

    alert("Comment edited successfuly");
    close_comment_modal();
    parent_comment_id = "";
    return;
  }

  alert("Commented successfuly");
  close_comment_modal();

  if (parent_comment_id == "") {
    await add_comment_to_div(res["data"]);
  } else {
    await add_comment_to_div(res["data"], parent_comment_id);
  }
  add_event_to_reply_btns();
  parent_comment_id = "";
  parent_email = "";
};

const init_comment_modal = () => {
  // close comment modal
  document
    .getElementById("close-comment-modal")
    .addEventListener("click", close_comment_modal);

  add_event_to_reply_btns();

  document
    .getElementById("submit-comment")
    .addEventListener("click", submit_comment);
  document
    .getElementById("preview-comment")
    .addEventListener("click", preview_comment);
  document
    .getElementById("edit-comment")
    .addEventListener("click", edit_comment);
};

export default init_comment_modal;
