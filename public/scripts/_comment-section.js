import {
  add_event_to_reply_btns,
  add_event_to_edit_comment_div,
} from "./_comment-modal.js";
import { fetch_req } from "./_fetch.js";

let converter = new showdown.Converter({
  tables: true,
  simpleLineBreaks: true,
  setFlavor: "github",
});

const get_post_comments = async () => {
  let url = window.location.pathname;
  let res = await fetch(url + "/comments");
  res = await res.json();
  res = res["data"];
  //   console.log(res);

  return res;
};

const get_recursive_comments = async (commentID) => {
  let res = await fetch(`/comment/${commentID}/all`);
  res = await res.json();
  res = res["data"];
  // console.log(res);

  return res;
};

export const load_post_comments = async () => {
  let comments = [];
  try {
    comments = await get_post_comments();
    // console.log(comments);
  } catch (e) {
    console.log(e);
    return;
  }

  let comment_holder_div = document.getElementById(
    "comment-section-comment-holder"
  );
  comment_holder_div.innerHTML = "";

  for (let i = comments.length - 1; i >= 0; --i) {
    await add_comment_to_div(comments[i]);
  }

  add_events();
};

const add_events = () => {
  add_event_to_reply_btns();
  add_event_to_lke_btns();
  add_event_to_edit_comment_div();
};

export const add_comment_to_div = async (comment, parent_comment_id = "") => {
  let root_comment = "",
    root_style = "";
  if (parent_comment_id == "") {
    root_comment = "root-comment";
    parent_comment_id = "comment-section-comment-holder";
    root_style = 'style="border: none"';
  }
  let comment_holder_div = document.getElementById(parent_comment_id);

  let content = converter.makeHtml(comment["text"]);
  content = filterXSS(content);

  let check_show_btn_hide = "";
  if (comment["comment_count"] == 0) {
    check_show_btn_hide = "hide";
  }

  let is_liked = "";
  if (window.db_user.liked_comments.indexOf(comment["_id"]) != -1) {
    is_liked = "select-btn";
  }

  let can_edit = "display:none";
  if (window.app_user.uid == comment.author_uid) {
    can_edit = "";
  }

  let temp = `<div class="comment-holder ${root_comment}" ${root_style} >
              <div class="show-more-comments ${check_show_btn_hide}" is_open="false" comment-id="${comment["_id"]}" >
                <button class="show-hide-btn hide">
                  <i class="fa-solid fa-minus"></i>
                </button>

                <button class="show-hide-btn">
                  <i class="fa-solid fa-plus"></i>
                </button>
              </div>

              <div class="comment">
                <div class="comment-profile">
                  <div class="comment-profile-image">
                    <img src="${comment["author_image_url"]}" alt="" />
                  </div>
                  <a class="comment-profile-email" href="/user/${comment["author_uid"]}"
                    >${comment["created_by"]}</a
                  >
                </div>

                <div class="comment-text">
                ${content}
                </div>

                <div class="comment-button-holder">
                  <button class="comment-like ${is_liked} " comment-ID="${comment["_id"]}">
                    <i class="fa-solid fa-heart"></i>
                    <div class="comment-like-count">${comment["like_count"]}</div>
                    like
                  </button>
                  <button 
                    class="comment-reply"
                    parent-comment-ID="${comment["_id"]}"
                    parent-comment-author="${comment["created_by"]}"
                  >
                    <i class="fa-solid fa-comment"></i> ${comment["comment_count"]} Reply
                  </button>

                  <button class="comment-edit" 
                  comment-ID="${comment["_id"]}"
                  style="${can_edit}"
                  >
                  <i class="fa-solid fa-pen-to-square"></i>
                    Edit
                  </button>
                </div>
              </div>
              <div id="${comment["_id"]}" ></div>
            </div>`;

  comment_holder_div.innerHTML = temp + comment_holder_div.innerHTML;
};

export const hide_recursive_comments = (comment_id) => {
  document.getElementById(comment_id).innerHTML = "";
  add_event_to_reply_btns();
};

export const show_recursive_comments = async (comment_id) => {
  let comments = await get_recursive_comments(comment_id);
  // console.log(comments);

  document.getElementById(comment_id).innerHTML = "";
  for (let i = comments.length - 1; i >= 0; --i) {
    await add_comment_to_div(comments[i], comment_id);
  }

  add_events();
};

const update_comment_div = (elem, num = 0) => {
  elem.classList.toggle("select-btn");
  let cnt_div = elem.getElementsByClassName("comment-like-count")[0];
  let cnt = Number(cnt_div.innerText);
  cnt_div.innerText = cnt + num;
};

const like_a_comment = async (e) => {
  let elem = e.currentTarget;
  let comment_id = elem.getAttribute("comment-ID");
  // console.log(comment_id);

  let res = await fetch_req(`/comment/${comment_id}/like`, "POST", {});
  res = await res.json();

  if (res.success === false) {
    if (res.msg == "Already liked comment") {
      unlike_comment(comment_id, elem);
      return;
    }
    alert(res.msg);
    return;
  }

  update_comment_div(elem, 1);
  alert("liked successfully");
};

const unlike_comment = async (comment_id, elem) => {
  let unlike_comm = confirm("Do you want to unlike ?");

  if (!unlike_comm) return;

  let unlike_res = await fetch_req(`/comment/${comment_id}/unlike`, "POST", {});
  unlike_res = await unlike_res.json();

  if (unlike_res.success) {
    alert(unlike_res.msg);
    return;
  }

  update_comment_div(elem, -1);
  alert("unliked successfully");
};

const add_event_to_lke_btns = () => {
  let like_btns = document.getElementsByClassName("comment-like");
  for (let i = 0; i < like_btns.length; ++i) {
    like_btns[i].removeEventListener("click", like_a_comment);
  }
  for (let i = 0; i < like_btns.length; ++i) {
    like_btns[i].addEventListener("click", like_a_comment);
  }
};
