const fetch_image_req = async (url, fetch_method, data) => {
  return await fetch(url, {
    method: fetch_method,
    redirect: "follow",
    referrerPolicy: "no-referrer",
    body: data,
  });
};

export const init_image_widget = () => {
  document
    .getElementsByClassName("floating-icon")[0]
    .addEventListener("click", toggle_widget);
};

const toggle_widget = () => {
  document.getElementById("open-widget-icon").classList.toggle("hide");
  document.getElementById("close-widget-icon").classList.toggle("hide");
  document.getElementsByClassName("content-holder")[0].classList.toggle("hide");

  document
    .getElementById("image-upload-btn")
    .addEventListener("click", upload_image);
};

const upload_image = async () => {
  let file_inp = document.getElementById("image-file-inp");

  document.getElementById("image-link-output").innerText = "uploading...";

  var data = new FormData();
  data.append("file_input", file_inp.files[0]);
  data.append("user", "hubot");

  let res = await fetch_image_req("/image-upload", "POST", data);
  res = await res.json();
  // console.log(res);

  if (res["success"] == false) {
    document.getElementById("image-link-output").innerText = "Failed to upload";
    alert(res["msg"]);
    return;
  }

  document.getElementById("image-link-output").innerText = res["url"];

  navigator.clipboard.writeText(res["url"]).then(
    () => {
      alert("link copied to clipboard");
    },
    () => {
      console.error("Failed to copy");
      alert(`link: ${res["url"]}`);
    }
  );
};
