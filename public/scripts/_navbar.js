export const toggle_profile_nav = () => {
  document
    .getElementsByClassName("profile-navs")[0]
    .classList.toggle("profile-nav-show");
};
export const show_loggedin_components = (user_data) => {
  // console.log(user_data);

  document.getElementById("g-login").classList.add("hide");

  document.getElementById("create").classList.remove("hide");
  document.getElementsByClassName("profile-holder")[0].classList.remove("hide");

  document.getElementById("profile-image").src = user_data["photoURL"];
  document.getElementById("profile-name").innerText = user_data["name"];
  document.getElementById("profile-email").innerText = user_data["email"];
  document.getElementsByClassName("detail")[0].onclick = () => {
    window.location = `/user/${user_data["uid"]}`;
  };
};

export const hide_loggedin_components = () => {
  document.getElementById("g-login").classList.remove("hide");
  document.getElementById("create").classList.add("hide");
  document.getElementsByClassName("profile-holder")[0].classList.add("hide");

  document.getElementById("profile-image").src = "";
  document.getElementById("profile-name").innerText = "user_name";
  document.getElementById("profile-email").innerText = "user_email";
  document.getElementById("profile-email").onclick = () => {
    window.location = "";
  };
};

const show_private_divs = () => {
  let divs = document.getElementsByClassName("private");
  let n = divs.length;

  for (let i = 0; i < n; ++i) {
    divs[i].style.display = "flex";
  }
};

const hide_private_divs = () => {
  let divs = document.getElementsByClassName("private");
  let n = divs.length;

  for (let i = 0; i < n; ++i) {
    divs[i].style.display = "none";
  }
};

export const check_profile_private = async (user) => {
  let url = window.location;

  if (user) {
    let db_user = await fetch(url + "/data");
    db_user = await db_user.json();

    if (db_user["data"].uid == user.uid) {
      show_private_divs();
      return;
    }
  }
  hide_private_divs();
};

export const check_blog_page_private = (user, author_uid) => {
  if (user) {
    if (author_uid == user.uid) {
      show_private_divs();
      return;
    }
  }
  hide_private_divs();
};
