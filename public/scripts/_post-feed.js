const search_by_tag = (e) => {
  let elem = e.currentTarget;
  // console.log(elem.innerText);

  let val = elem.innerText.replaceAll("#", "");
  // console.log(val);

  window.location = `/?search=${val}`;
};
export const add_events_to_tags = () => {
  let tag_divs = document.getElementsByClassName("tag");

  for (let i = 0; i < tag_divs.length; ++i) {
    tag_divs[i].removeEventListener("click", search_by_tag);
  }
  for (let i = 0; i < tag_divs.length; ++i) {
    tag_divs[i].addEventListener("click", search_by_tag);
  }
};
export const create_featured_posts = (data, post_id_list = []) => {
  let output_div = document.getElementsByClassName("featured-posts")[0];
  output_div.innerHTML = "";

  if (data.length == 0) {
    output_div.innerHTML = "<p>No content available here.</p>";
    return;
  }

  let temp = "";
  for (let i = 0; i < data.length; ++i) {
    let post = data[i];

    let like_class = "";
    if (post_id_list.indexOf(post["_id"]) != -1) {
      like_class = "select-btn";
    }

    let tags = `<div class="tags">`;
    for (let j = 0; j < post.tags.length; ++j) {
      tags += `<div class="tag">#${post.tags[j]}</div>`;
    }
    tags += "</div>";

    temp += `
      <div class="post">
      <div class="post-image-holder">
          <img src="${post["cover_image_link"]}" alt="" class="cover-image" />
        </div>
            <div class="post-details">
              <a class="post-title" href="/post/${post["_id"]}"
                >${post["title"]}</a>
                <a class="author" href="/user/${post["author_uid"]}" target="_blank"
                >by: ${post["created_by"]}</a
              >
              ${tags}
              <div class="post-info">
                <div class="upvotes ${like_class}"><i class="fa-regular fa-heart"></i> ${post["like_count"]} likes</div>
                <div class="comments"><i class="fa-regular fa-message"></i> ${post["comment_count"]} comments</div>
              </div>
            </div>
          </div>
      `;
  }

  output_div.innerHTML = temp;

  add_events_to_tags();
};

export const create_image_uploads = (data) => {
  let output_div = document.getElementsByClassName("featured-posts")[0];
  output_div.innerHTML = "";

  if (data.length == 0) {
    output_div.innerHTML = "<p>No content available here.</p>";
    return;
  }

  let temp = "";
  for (let i = 0; i < data.length; ++i) {
    let join_year = new Date(data[i]["created_at"]);
    join_year = join_year.toLocaleString().split(",")[0];
    temp += `
      <div class="post">
      <div class="post-image-holder">
          <img src="${data[i]["upload_link"]}" alt="" class="cover-image" />
        </div>
            <div class="post-details">
              <div class="image-upload-url-text">${data[i]["upload_link"]}</div>
              <div class="image-upload-date">Uploaded at: ${join_year}</div>
            </div>
          </div>
      `;
  }

  output_div.innerHTML = temp;
};
