const search_func = async () => {
  let inp = document.getElementById("search-inp").value;
  //   console.log(inp);
  if (inp.trim() == "") {
    window.location = "/";
    return;
  }
  window.location = `/?search=${inp}`;
};

export const init_search = () => {
  try {
    document
      .getElementById("search-btn")
      .addEventListener("click", search_func);
  } catch (error) {
    console.log("Search not avaialble on this window");
  }
};
