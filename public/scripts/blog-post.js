import { auth_init } from "./_auth.js";
import { fetch_req } from "./_fetch.js";
import { check_blog_page_private } from "./_navbar.js";
import { init_image_widget } from "./_image-widget.js";
import init_comment_modal from "./_comment-modal.js";
import { load_post_comments } from "./_comment-section.js";
import { init_search } from "./_search.js";
import { add_events_to_tags } from "./_post-feed.js";
let post_data = null,
  db_user_data = null,
  app_user = null;
let converter;
let url = window.location.pathname;

window.onload = async () => {
  try {
    app_user = await auth_init();
    window.app_user = app_user;

    let db_user = await fetch(`/user/${window.app_user.uid}/data`);
    db_user = await db_user.json();
    db_user = db_user["data"];
    // console.log(db_user);

    window.db_user = db_user;

    init_comment_modal();
    init_image_widget();
  } catch (error) {
    console.log(error);
  }
  init_search();

  document.getElementsByClassName("post-holder")[0].classList.remove("shadow");
  document.getElementById("like-btn").addEventListener("click", like_post);
  document
    .getElementById("bookmark-post")
    .addEventListener("click", bookmark_post);
  document
    .getElementById("archive-post-btn")
    .addEventListener("click", archive_post);

  converter = new showdown.Converter({
    tables: true,
    simpleLineBreaks: true,
  });

  post_data = await fetch_post();
  check_blog_page_private(app_user, post_data["author_uid"]);
  document.getElementById("edit-post-btn").onclick = () => {
    window.location = `/create/?postID=${post_data["_id"]}`;
  };

  await load_post_comments();
};

const fetch_post = async () => {
  let post_get_fetch = await fetch(url + "/data");
  post_get_fetch = await post_get_fetch.json();
  window.postID = post_get_fetch["data"]["_id"];

  // console.log("Post data:");
  // console.log(post_get_fetch["data"]);

  make_post(post_get_fetch["data"]);
  await build_like_bookmark(post_get_fetch["data"]);
  add_events_to_tags();
  return post_get_fetch["data"];
};

const make_post = (data) => {
  document.getElementsByTagName("title")[0].innerText = data["title"];

  document.getElementById("post-title").innerText = data["title"];
  document.getElementsByClassName(
    "author"
  )[0].href = `/user/${data["author_uid"]}`;
  document.getElementsByClassName(
    "author"
  )[0].innerText = `by: ${data["created_by"]}`;
  document.getElementById("cover-image").src = data["cover_image_link"];

  let tags_div = document.getElementById("tags-out");
  tags_div.innerHTML = "";

  let temp = "";
  for (let i = 0; i < data.tags.length; ++i) {
    temp += `<div class="tag">#${data.tags[i]}</div>`;
  }
  tags_div.innerHTML = temp;

  document.getElementById("like-count").innerText = data["like_count"];
  document.getElementById("comment-count").innerText = data["comment_count"];

  let content = converter.makeHtml(data["content"]);
  content = filterXSS(content);

  document.getElementById("content").innerHTML = content;
};

const build_like_bookmark = async (post_data) => {
  if (app_user == null) return;
  db_user_data = await fetch(`/user/${app_user.uid}/data`);
  db_user_data = await db_user_data.json();
  db_user_data = db_user_data["data"];

  // console.log("DB user:");
  // console.log(db_user_data);

  let user = db_user_data;

  // console.log(db_user_data);
  if (user.liked_posts.indexOf(post_data["_id"]) != -1) {
    document.getElementById("like-btn").classList.add("select-btn");
  } else {
    document.getElementById("like-btn").classList.remove("select-btn");
  }

  if (user.bookmarked_posts.indexOf(post_data["_id"]) != -1) {
    document.getElementById("bookmark-post").classList.add("select-btn");
  } else {
    document.getElementById("bookmark-post").classList.remove("select-btn");
  }

  if (post_data["is_archived"] == true) {
    document.getElementById("archive-post-btn").classList.add("select-btn");
  } else {
    document.getElementById("archive-post-btn").classList.remove("select-btn");
  }
};

const like_post = async () => {
  let res = await fetch_req(url + "/like", "POST", {});
  res = await res.json();

  if (res.success === false) {
    if (res.msg == "Already liked") {
      unlike_post();
      return;
    }
    alert(res.msg);
    return;
  }

  await fetch_post();
  alert("liked successfully");
};

const unlike_post = async () => {
  let unlike_comm = confirm("Do you want to unlike ?");

  if (!unlike_comm) return;

  let unlike_res = await fetch_req(url + "/unlike", "POST", {});
  unlike_res = await unlike_res.json();

  if (unlike_res.success) {
    alert(unlike_res.msg);
    return;
  }

  await fetch_post();
  alert("unliked successfully");
};

const bookmark_post = async () => {
  let res = await fetch_req(url + "/add-to-bookmark", "POST", {});
  res = await res.json();

  if (res.success === false) {
    if (res.msg == "Already bookmarked") {
      remove_bookmark_post();
      return;
    }
    alert(res.msg);
    return;
  }

  await fetch_post();
  alert("bookmarked successfully");
};

const remove_bookmark_post = async () => {
  let unlike_comm = confirm("Do you want to remove post from bookmark ?");

  if (!unlike_comm) return;

  let unlike_res = await fetch_req(url + "/remove-from-bookmark", "POST", {});
  unlike_res = await unlike_res.json();

  if (unlike_res.success) {
    alert(unlike_res.msg);
    return;
  }

  await fetch_post();
  alert("removed from bookmark successfully");
};

const archive_post = async () => {
  let res = await fetch_req(url + "/add-to-archive", "POST", {});
  res = await res.json();

  if (res.success === false) {
    if (res.msg == "Already archived") {
      remove_archived_post();
      return;
    }
    alert(res.msg);
    return;
  }

  await fetch_post();
  alert("archived successfully");
};

const remove_archived_post = async () => {
  let unlike_comm = confirm("Do you want to remove post from archive ?");

  if (!unlike_comm) return;

  let unlike_res = await fetch_req(url + "/remove-from-archive", "POST", {});
  unlike_res = await unlike_res.json();

  if (unlike_res.success) {
    alert(unlike_res.msg);
    return;
  }

  await fetch_post();
  alert("removed from archive successfully");
};
