import { auth_init } from "./_auth.js";
import { fetch_req } from "./_fetch.js";
import { init_image_widget } from "./_image-widget.js";

let converter = new showdown.Converter({
  tables: true,
  simpleLineBreaks: true,
  setFlavor: "github",
});
let postID = undefined;
// converter.setFlavor("github");

window.onload = async () => {
  try {
    init_image_widget();
    await auth_init();
  } catch (error) {
    console.log(error);
  }

  let url_string = window.location;
  let url = new URL(url_string);
  postID = url.searchParams.get("postID");
  // console.log(postID);

  if (postID != null) {
    await fill_post_details();
  }

  document.getElementById("preview").onclick = () => {
    document.getElementsByClassName("post-holder")[0].classList.remove("hide");
    document.getElementById("edit-holder").classList.add("hide");

    fill_preview_post();
  };

  document.getElementById("edit").onclick = () => {
    document.getElementsByClassName("post-holder")[0].classList.add("hide");

    document.getElementById("edit-holder").classList.remove("hide");
  };

  document.getElementById("publish").addEventListener("click", publish_post);
};

const fill_post_details = async () => {
  let post_data = await fetch(`/post/${postID}/data`);
  post_data = await post_data.json();
  if (post_data["success"] == false) {
    alert("Post not found");
    postID = undefined;
    window.location = "/create";
    return;
  }
  post_data = post_data["data"];
  // console.log(post_data);
  document.getElementById("post-title").value = post_data["title"];
  document.getElementById("cover-image-link-inp").value =
    post_data["cover_image_link"];
  document.getElementById("tag-input").value = post_data["tags"].join(",");

  document.getElementById("mark-content").value = post_data["content"];
};

const publish_post = async () => {
  let post = {};
  post["title"] = document.getElementById("post-title").value;
  post["cover_image_link"] = document.getElementById(
    "cover-image-link-inp"
  ).value;
  post["tags"] = document.getElementById("tag-input").value.split(",");
  post["tags"] = post["tags"].map((e) => e.trim());

  post["content"] = document.getElementById("mark-content").value;

  console.log(post);

  let method_url = "/create";
  if (postID != undefined) {
    method_url = "/create/edit";
    post["_id"] = postID;
  }

  let res = await fetch_req(method_url, "POST", post);
  res = await res.json();

  if (res["success"] === false) {
    alert(res["msg"]);
    return;
  }
  alert("Post created/updated successfully!");
  console.log(res);
};

const fill_preview_post = () => {
  document.getElementById("cover-image").src = document.getElementById(
    "cover-image-link-inp"
  ).value;

  document.getElementById("post-title-display").innerText =
    document.getElementById("post-title").value;

  let input_text = document.getElementById("mark-content").value;
  let content = converter.makeHtml(input_text);

  let tags = document.getElementById("tag-input").value.split(",");
  tags = tags.map((e) => e.trim());

  let tags_div = document.getElementById("tags-out");
  tags_div.innerHTML = "";
  for (let i = 0; i < tags.length; ++i) {
    tags_div.innerHTML += `<div class="tag">#${tags[i]}</div>`;
  }

  let content_div = document.getElementById("content");
  content = filterXSS(content);

  content_div.innerHTML = content;
};
