import { auth_init } from "./_auth.js";
import { create_featured_posts } from "./_post-feed.js";
import { init_search } from "./_search.js";
import { fetch_req } from "./_fetch.js";
let posts = [];

window.onload = async () => {
  let app_user = await auth_init();

  let like_post_ids = [];
  if (app_user != null) {
    let db_user = await fetch(`/user/${app_user["uid"]}/data`);
    db_user = await db_user.json();
    db_user = db_user["data"];

    if (db_user) {
      like_post_ids = db_user["liked_posts"];
      window.like_post_ids = like_post_ids;
    }
  }

  let url_string = window.location;
  let url = new URL(url_string);
  let search_inp = url.searchParams.get("search");

  let posts_res = await fetch("/featured-posts/new");

  if (search_inp && search_inp.length >= 3) {
    posts_res = await fetch_req("/search/posts", "POST", { title: search_inp });
    document.getElementById("search-inp").value = search_inp;
  }

  posts_res = await posts_res.json();

  posts = posts_res["data"];
  // console.log(posts);

  create_featured_posts(posts, like_post_ids);
  init_search();
};
