import { check_profile_private } from "./_navbar.js";
import { auth_init } from "./_auth.js";
import { create_featured_posts, create_image_uploads } from "./_post-feed.js";
import { init_search } from "./_search.js";

let posts = [],
  liked_post_ids = [];
let url = window.location;
let liked_comment_ids = [];

let converter = new showdown.Converter({
  tables: true,
  simpleLineBreaks: true,
  setFlavor: "github",
});

window.onload = async () => {
  let app_user = await auth_init();
  await check_profile_private(app_user);

  let res = await fetch(url + "/data");
  res = await res.json();
  //  console.log(res["data"]);
  init_search();
  if (app_user) {
    let curr_user_db_data = await fetch(`/user/${app_user["uid"]}/data`);
    curr_user_db_data = await curr_user_db_data.json();

    liked_post_ids = curr_user_db_data["data"]["liked_posts"];
    liked_comment_ids = curr_user_db_data["data"]["liked_comments"];
  }

  fill_profile_card(res["data"]);

  document
    .getElementById("profle-liked-posts")
    .addEventListener("click", show_liked_posts);

  document
    .getElementById("profle-all-posts")
    .addEventListener("click", show_all_posts);

  document
    .getElementById("profile-bookmarked-posts")
    .addEventListener("click", show_bookmarked_posts);
  document
    .getElementById("profile-archived-posts")
    .addEventListener("click", show_archived_posts);

  document
    .getElementById("profile-uploaded-images")
    .addEventListener("click", show_uploaded_images);

  document
    .getElementById("profle-all-comments")
    .addEventListener("click", show_all_comments);
  document
    .getElementById("profle-liked-comments")
    .addEventListener("click", show_liked_comments);

  await show_all_posts();
};

const show_liked_posts = async () => {
  let posts_res = await fetch(url + "/liked-posts");
  posts_res = await posts_res.json();
  posts = posts_res["data"];

  create_featured_posts(posts, liked_post_ids);
};

const show_all_posts = async () => {
  let posts_res = await fetch(url + "/all-posts");
  posts_res = await posts_res.json();

  posts = posts_res["data"];

  create_featured_posts(posts, liked_post_ids);
};

const show_bookmarked_posts = async () => {
  let posts_res = await fetch(url + "/bookmarked-posts");
  posts_res = await posts_res.json();

  posts = posts_res["data"];

  create_featured_posts(posts, liked_post_ids);
};

const show_archived_posts = async () => {
  let posts_res = await fetch(url + "/archived-posts");
  posts_res = await posts_res.json();

  posts = posts_res["data"];

  create_featured_posts(posts, liked_post_ids);
};

const show_uploaded_images = async () => {
  let img_res = await fetch(url + "/uploads");
  img_res = await img_res.json();
  console.log(img_res);

  create_image_uploads(img_res["data"]);
};

const create_comments = async (data, comment_id_list = []) => {
  let output_div = document.getElementsByClassName("featured-posts")[0];
  output_div.innerHTML = "";

  if (data.length == 0) {
    output_div.innerHTML = "<p>No content available here.</p>";
    return;
  }

  let posts = {};

  let temp = "";
  for (let i = 0; i < data.length; ++i) {
    let comment = data[i];

    let like_class = "";
    if (comment_id_list.indexOf(comment["_id"]) != -1) {
      like_class = "select-btn";
    }
    let content = converter.makeHtml(comment["text"]);
    content = filterXSS(content);

    let post = undefined;
    if (posts[comment["post_id"]]) {
      post = posts[comment["post_id"]];
    } else {
      let post_res = await fetch(`/post/${comment["post_id"]}/data`);
      post_res = await post_res.json();
      post = post_res["data"];
      posts[comment["post_id"]] = post;
    }

    temp += `
      <div class="post">
            <div class="post-details">
            <a class="post-title" href="/post/${post["_id"]}"
                >${post["title"]}</a>
                <a class="author" href="/post/${comment["post_id"]}" target="_blank"
                >on post: ${comment["post_id"]}</a
              >
              <div class="comment-content">
              ${content}
              </div>
              <div class="post-info">
                <div class="upvotes ${like_class}"><i class="fa-regular fa-heart"></i> ${comment["like_count"]} likes</div>
                <div class="comments"><i class="fa-regular fa-message"></i> ${comment["comment_count"]} comments</div>
              </div>
            </div>
          </div>
      `;
  }

  output_div.innerHTML = temp;
};

const show_all_comments = async () => {
  let comment_res = await fetch(url + "/all-comments");
  comment_res = await comment_res.json();

  let comments = comment_res["data"];

  create_comments(comments, liked_comment_ids);
};

const show_liked_comments = async () => {
  let comment_res = await fetch(url + "/liked-comments");
  comment_res = await comment_res.json();

  let comments = comment_res["data"];

  create_comments(comments, liked_comment_ids);
};

const fill_profile_card = (data) => {
  let join_year = new Date(data["created_at"]);
  join_year = join_year.toLocaleString().split(",")[0];

  document.getElementById("card-profile-image").src = data["photoURL"];
  document.getElementById("card-name").innerText = data["name"];

  document.getElementsByTagName(
    "title"
  )[0].innerText = `${data["name"]} | Folding Paper`;

  document.getElementById("card-email").innerText = data["email"];
  document.getElementById("card-year").innerText = `Joined in: ${join_year}`;
};
