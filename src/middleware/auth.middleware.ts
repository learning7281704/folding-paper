import admin from "firebase-admin";
import { initializeApp } from "firebase-admin/app";
import { getAuth } from "firebase-admin/auth";
import mongo_client from "./../services/db.service.js";
import dotenv from "dotenv";
import cookie from "cookie";
dotenv.config();

import serviceAccount from "../../firebase-keys.json" assert { type: "json" };
// const serviceAccount = JSON.parse(process.env.FIREBASE_JSON);

const firebase_app = initializeApp({
  credential: admin.credential?.cert(serviceAccount as admin.ServiceAccount),
  storageBucket: "folding-paper.appspot.com",
});

const createNewUser = async (data: any) => {
  data["created_at"] = new Date();
  data["liked_posts"] = [];
  data["liked_comments"] = [];
  data["bookmarked_posts"] = [];

  try {
    let db = mongo_client.db("folding-paper");
    let user_DB = await db.collection("users").findOne({ uid: data["uid"] });
    if (user_DB != null) return;

    let res = await db
      .collection("users")
      .findOneAndUpdate(
        { email: data["email"] },
        { $set: { ...data } },
        { upsert: true }
      );

    console.log(`user created: ${data["email"]}`);
  } catch (e) {
    console.log("user creation failed");
    console.log(e);
  }
};

export const fetchLoggedInUser = async (req: any, res: any, next: any) => {
  req.userData = undefined;
  if (!req.headers.cookie) {
    console.log("no cookie");
    next();
    return;
  }

  try {
    let cookieData = cookie.parse(req.headers.cookie);
    let idToken = cookieData["idToken"];
    // console.log(idToken);

    let decodedToken = await getAuth().verifyIdToken(idToken);
    // console.log(decodedToken);

    let data: any = {};
    data.email = decodedToken.email;
    data.name = decodedToken.name;
    data.uid = decodedToken.uid;
    data.photoURL = decodedToken.picture;
    // console.log(data);

    req.userData = data;

    console.log(`user already logged in: ${data.email}`);

    await createNewUser(data);
    next();
  } catch (error) {
    console.log(error);
    console.log("auth error");
    next();
  }
};

export const check403 = async (req: any, res: any, next: any) => {
  if (req.userData === undefined) {
    res.sendFile("403.html", { root: "./public" });
    return;
  }
  next();
};
