import mongo_client from "./../services/db.service.js";

const updateUser = async (req: any, res: any, next: any) => {
  let data = req.userData;
  if (data === undefined) {
    next();
    return;
  }

  data["created_at"] = new Date();

  try {
    let db = mongo_client.db("folding-paper");
    let res = await db
      .collection("users")
      .findOneAndUpdate(
        { email: data["email"] },
        { $set: { ...data } },
        { upsert: true }
      );

    console.log(`user updated: ${data["email"]}`);
  } catch (e) {
    console.log("user updated failed");
    console.log(e);
  }
  next();
};

export default updateUser;
