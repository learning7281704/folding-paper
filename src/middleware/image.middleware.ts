export const image_file_check = (req: any, res: any, next: any) => {
  const image: any = req?.files?.file_input;
  //   console.log(image);

  const array_of_allowed_file_types = [
    "image/png",
    "image/jpeg",
    "image/jpg",
    "image/gif",
  ];

  // Allowed file size in mb
  const allowed_file_size = 5;

  // Check if the uploaded file is allowed
  if (array_of_allowed_file_types.indexOf(image.mimetype) == -1) {
    res.status(401);
    res.json({ success: false, msg: "Invalid file type" });
    return;
  }

  if (image.size / (1024 * 1024) > allowed_file_size) {
    res.status(401);
    res.json({
      success: false,
      msg: `File size greater than ${allowed_file_size}MB`,
    });
    return;
  }
  next();
};
