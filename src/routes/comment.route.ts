import express from "express";
import mongo_client from "./../services/db.service.js";
import { ObjectId } from "mongodb";

// middleware
import { fetchLoggedInUser } from "../middleware/auth.middleware.js";
import comment_schema from "../validation-schema/comment.schema.js";

const comment_router = express.Router({ mergeParams: true });

comment_router.post("/", fetchLoggedInUser, async (req: any, res: any) => {
  let inp_data = req.body;

  if (req.userData == undefined) {
    res.status(401);
    res.json({ success: false, msg: "user not loggedin" });
    return;
  }

  let temp_data = {
    text: inp_data["text"],
    post_id: inp_data["post_id"],
    parent_comment_id: inp_data["parent_comment_id"],
    parent_author_email: inp_data["parent_author_email"],
  };

  let validation = comment_schema.validate(inp_data);
  if (validation.error != null) {
    res.status(400);
    res.json({ success: false, msg: validation.error.details[0].message });
    return;
  }

  let db = mongo_client.db("folding-paper");

  //check if parent_comment_id exists and it's author is correct
  if (inp_data["parent_comment_id"].length != 0) {
    try {
      let post_res: any = await db
        .collection("comments")
        .findOne({ _id: new ObjectId(inp_data["parent_comment_id"]) });

      if (post_res["created_by"] != inp_data["parent_author_email"]) {
        throw Error("email invalid");
      }
    } catch (e) {
      console.log(e);
      res.status(404);
      res.json({ success: false, msg: "invalid data" });
      return;
    }
  }

  // check if post exists
  try {
    let post_res = await db
      .collection("posts")
      .findOne({ _id: new ObjectId(inp_data["post_id"]) });
  } catch (e) {
    console.log(e);
    res.status(404);
    res.json({ success: false, msg: "Post doesn't exist" });
    return;
  }

  let data: any = {
    ...temp_data,
    author_image_url: req.userData.photoURL,
    author_uid: req.userData.uid,
    created_by: req.userData["email"],
    created_at: new Date(),
    modified_at: new Date(),
    like_count: 0,
    comment_count: 0,
    is_archived: false,
  };
  //   console.log(data);

  const session = mongo_client.startSession();
  let error_msg = "server error";
  try {
    session.startTransaction();
    let comm_insert = await db.collection("comments").insertOne(data);

    // increase comment count
    if (data["parent_comment_id"] != "") {
      await db
        .collection("comments")
        .updateOne(
          { _id: new ObjectId(data["parent_comment_id"]) },
          { $inc: { comment_count: 1 } }
        );
    } else {
      await db
        .collection("posts")
        .updateOne(
          { _id: new ObjectId(data["post_id"]) },
          { $inc: { comment_count: 1 } }
        );
    }
    await session.commitTransaction();

    data["_id"] = comm_insert["insertedId"];

    console.log(`Comment created by: ${data["created_by"]}`);
    console.log("Transaction committed.");
  } catch (error) {
    await session.abortTransaction();

    console.log(error);
    res.status(500);
    res.json({ success: false, msg: error_msg });
    return;
  } finally {
    await session.endSession();
  }

  res.json({ success: true, data });
});

comment_router.post("/edit", fetchLoggedInUser, async (req: any, res: any) => {
  let inp_data = req.body;

  if (req.userData == undefined) {
    res.status(401);
    res.json({ success: false, msg: "user not loggedin" });
    return;
  }

  let db = mongo_client.db("folding-paper");

  // check if comment exists
  try {
    let comm_res = await db
      .collection("comments")
      .findOne({ _id: new ObjectId(inp_data["parent_comment_id"]) });

    if (comm_res == null) {
      throw Error("comment doesn't exist");
    }
  } catch (e) {
    console.log(e);
    res.status(404);
    res.json({ success: false, msg: "comment doesn't exist" });
    return;
  }

  const session = mongo_client.startSession();
  let error_msg = "server error";
  try {
    session.startTransaction();
    let comm_update = await db
      .collection("comments")
      .updateOne(
        { _id: new ObjectId(inp_data["parent_comment_id"]) },
        { $set: { text: inp_data["text"] } }
      );
    console.log(comm_update);

    await session.commitTransaction();

    console.log("Transaction committed.");
  } catch (error) {
    await session.abortTransaction();

    console.log(error);
    res.status(500);
    res.json({ success: false, msg: error_msg });
    return;
  } finally {
    await session.endSession();
  }

  res.json({ success: true });
});

comment_router.get("/:commentID", async (req: any, res: any) => {
  let comment_id = req.params.commentID;

  let comment_res: any = {};
  try {
    let db = mongo_client.db("folding-paper");
    comment_res = await db
      .collection("comments")
      .findOne({ _id: new ObjectId(comment_id) });
  } catch (e) {
    console.log(e);
    res.status(500);
    res.json({ success: false, msg: "server error", data: {} });
    return;
  }

  res.json({ success: true, data: comment_res });
});

comment_router.get("/:commentID/all", async (req: any, res: any) => {
  let comment_id = req.params.commentID;

  let comment_res: any = [];
  try {
    let db = mongo_client.db("folding-paper");
    comment_res = db
      .collection("comments")
      .find({ parent_comment_id: comment_id })
      .sort({ modified_at: -1 });
    comment_res = await comment_res.toArray();
    // console.log(comment_res);
  } catch (e) {
    console.log(e);
    res.status(500);
    res.json({ success: false, msg: "server error", data: {} });
    return;
  }

  res.json({ success: true, data: comment_res });
});

comment_router.post(
  "/:commentID/like",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let commentID: string = req.params?.commentID;

    if (req.userData === undefined) {
      res.json({ success: false, msg: "user not logged in" });
      return;
    }

    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
      session.startTransaction();

      let db = mongo_client.db(process.env.DBNAME);

      // check if user has already liked  the post
      let user_data: any = await db
        .collection("users")
        .findOne({ uid: req.userData.uid });

      if (Object.keys(user_data).indexOf("liked_comments") == -1) {
        user_data["liked_comments"] = [];
      }

      if (user_data?.liked_comments.indexOf(commentID) != -1) {
        error_msg = "Already liked comment";
        throw new Error("already liked comment");
      }

      // update like count
      await db
        .collection("comments")
        .updateOne(
          { _id: new ObjectId(commentID) },
          { $inc: { like_count: 1 } }
        );

      // add commentID to user doc
      await db
        .collection("users")
        .updateOne(
          { uid: req.userData.uid },
          { $push: { liked_comments: commentID } }
        );

      await session.commitTransaction();
      console.log("Transaction committed.");
    } catch (error) {
      await session.abortTransaction();

      console.log(error);
      res.status(500);
      res.json({ success: false, msg: error_msg });
      return;
    } finally {
      await session.endSession();
    }

    res.json({ sucess: true });
  }
);

comment_router.post(
  "/:commentID/unlike",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let commentID: string = req.params?.commentID;

    if (req.userData === undefined) {
      res.json({ success: false, msg: "user not logged in" });
      return;
    }

    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
      session.startTransaction();

      let db = mongo_client.db(process.env.DBNAME);

      // check if user has already liked  the post
      let user_data: any = await db
        .collection("users")
        .findOne({ uid: req.userData.uid });

      if (Object.keys(user_data).indexOf("liked_comments") == -1) {
        user_data["liked_comments"] = [];
      }

      if (user_data?.liked_comments.indexOf(commentID) == -1) {
        error_msg = "not liked yet";
        throw new Error("already liked yet");
      }

      // update like count
      await db
        .collection("comments")
        .updateOne(
          { _id: new ObjectId(commentID) },
          { $inc: { like_count: -1 } }
        );

      // add postID to user doc
      await db
        .collection("users")
        .updateOne(
          { uid: req.userData.uid },
          { $pull: { liked_comments: commentID } }
        );

      await session.commitTransaction();
      console.log("Transaction committed.");
    } catch (error) {
      await session.abortTransaction();

      console.log(error);
      res.status(500);
      res.json({ success: false, msg: error_msg });
      return;
    } finally {
      await session.endSession();
    }

    res.json({ sucess: true });
  }
);

export default comment_router;
