import express from "express";
import mongo_client from "./../services/db.service.js";
import path from "path";
import post_schema from "./../validation-schema/post.schema.js";
const __dirname = path.resolve();

// middleware
import {
  fetchLoggedInUser,
  check403,
} from "./../middleware/auth.middleware.js";
import { ObjectId } from "mongodb";

const create_router = express.Router({ mergeParams: true });

create_router.use("/", express.static(__dirname + "/public"));

create_router.get("/", fetchLoggedInUser, (req: any, res: any) => {
  res.sendFile("edit-blog.html", { root: "./public" });
});

create_router.post("/", fetchLoggedInUser, async (req: any, res: any) => {
  // console.log(req.userData);

  if (req.userData === undefined) {
    res.status(401);
    res.json({ success: false, msg: "unauthorized accesss" });
    return;
  }

  let inp_data = req.body;
  // console.log(inp_data);

  let validation = post_schema.validate(inp_data);
  if (validation.error != null) {
    res.status(400);
    res.json({ success: false, msg: validation.error.details[0].message });
    return;
  }

  let data = {
    ...inp_data,
    created_by: req.userData["email"],
    author_uid: req.userData["uid"],
    created_at: new Date(),
    modified_at: new Date(),
    like_count: 0,
    comment_count: 0,
    is_archived: false,
  };
  // console.log(data);

  try {
    let db = mongo_client.db("folding-paper");
    let res = await db.collection("posts").insertOne(data);

    console.log(`Post created by: ${data["created_by"]}`);
  } catch (e) {
    console.log(e);
    res.status(500);
    res.json({ success: false, msg: "server error" });
    return;
  }

  res.json({ success: true });
});

create_router.post("/edit", fetchLoggedInUser, async (req: any, res: any) => {
  // console.log(req.userData);

  if (req.userData === undefined) {
    res.status(401);
    res.json({ success: false, msg: "unauthorized accesss" });
    return;
  }

  let inp_data = req.body;
  // console.log(inp_data);

  let validation = post_schema.validate(inp_data);
  if (validation.error != null) {
    res.status(400);
    res.json({ success: false, msg: validation.error.details[0].message });
    return;
  }

  // check if post exists
  let post_res = undefined;
  try {
    let db = mongo_client.db("folding-paper");
    post_res = await db
      .collection("posts")
      .findOne({ _id: new ObjectId(inp_data["_id"]) });
    if (post_res == null) {
      res.status(404);
      res.json({ success: false, msg: "post not found" });
      return;
    }
  } catch (error) {
    console.log(error);
    res.status(500);
    res.json({ success: false, msg: "server error" });
    return;
  }

  if (post_res["author_uid"] != req.userData.uid) {
    res.status(401);
    res.json({ success: false, msg: "unauthorized accesss" });
    return;
  }

  let data = {
    ...inp_data,
    modified_at: new Date(),
  };
  delete data["_id"];
  // console.log(data);

  try {
    let db = mongo_client.db("folding-paper");
    let res = await db
      .collection("posts")
      .findOneAndUpdate(
        { _id: new ObjectId(inp_data["_id"]) },
        { $set: { ...data } }
      );
    // console.log(res);

    console.log(`Post edited by: ${req.userData.email}`);
  } catch (e) {
    console.log(e);
    res.status(500);
    res.json({ success: false, msg: "server error" });
    return;
  }

  res.json({ success: true });
});

export default create_router;
