import express from "express";
import mongo_client from "./../services/db.service.js";

const featured_router = express.Router({ mergeParams: true });

featured_router.get("/new", async (req: any, res: any) => {
  let res_data: any[] = [];
  try {
    let db = mongo_client.db("folding-paper");
    let res: any = db.collection("posts").find().sort({ modified_at: -1 });
    res_data = await res.toArray();

    // console.log(res);
  } catch (e) {
    console.log(e);
    res.status(500);
    res.json({ success: false, msg: "server error" });
    return;
  }

  res.json({ success: true, data: res_data });
});

export default featured_router;
