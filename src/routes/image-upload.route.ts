import express from "express";
import { v4 as uuidv4 } from "uuid";
import mongo_client from "./../services/db.service.js";
import { uploadImage } from "./../services/firebase.storage.service.js";

// middleware
import { fetchLoggedInUser } from "./../middleware/auth.middleware.js";
import { image_file_check } from "./../middleware/image.middleware.js";

const image_upload_router = express.Router({ mergeParams: true });

const store_image_link = async (user_email: any, image_link: string) => {
  let data = {
    user_email,
    upload_link: image_link,
    created_at: new Date(),
  };
  try {
    let db = mongo_client.db(process.env.DBNAME);

    await db.collection("uploads").insertOne(data);
    console.log(`image link uploaded to db by: ${user_email}`);
  } catch (e) {
    console.log("error: in image upload");

    console.log(e);
  }
};

image_upload_router.use(
  "/",
  [image_file_check, fetchLoggedInUser],
  async (req: any, res: any) => {
    if (req?.userData === undefined) {
      res.status(401);
      res.json({ success: false, msg: "unauthorised access" });
      return;
    }
    let file: any = req?.files?.file_input;

    let file_name: any = file["name"];
    let file_extension = file_name.split(".");
    file_extension = file_extension[file_extension.length - 1];
    // console.log(file_extension);

    let url = await uploadImage(
      `${uuidv4()}.${file_extension}`,
      file.data,
      file_extension
    );
    //   console.log(url);

    await store_image_link(req.userData.email, url);

    res.json({ success: true, url });
  }
);

export default image_upload_router;
