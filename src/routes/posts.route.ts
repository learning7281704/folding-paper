import express from "express";
import mongo_client from "./../services/db.service.js";
import { ObjectId } from "mongodb";
import path from "path";
const __dirname = path.resolve();
// console.log(__dirname);

// middleware
import { fetchLoggedInUser } from "../middleware/auth.middleware.js";

const post_router = express.Router({ mergeParams: true });

post_router.use("/", express.static(__dirname + "/public"));

post_router.get("/", (req: any, res: any) => {
  res.redirect("/");
});

post_router.get("/:postID", async (req: any, res: any) => {
  let postID: string = req.params?.postID;

  try {
    let db = mongo_client.db(process.env.DBNAME);

    let res = await db
      .collection("posts")
      .findOne({ _id: new ObjectId(postID) });
    // console.log(res);

    if (res === null) throw new Error("post not found");
  } catch (error) {
    console.log(error);
    res.status(404);
    res.sendFile("404.html", { root: "./public" });
    return;
  }

  res.sendFile("blog-post.html", { root: "./public" });
});

post_router.get(
  "/:postID/data",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let postID: string = req.params?.postID;

    let res_data: any;
    try {
      let db = mongo_client.db(process.env.DBNAME);

      res_data = await db
        .collection("posts")
        .findOne({ _id: new ObjectId(postID) });
      // console.log(res);
    } catch (error) {
      console.log(error);
      res.status(404);
      res.json({ success: false });
      return;
    }

    if (
      res_data["is_archived"] == true &&
      req?.userData?.uid != res_data["author_uid"]
    ) {
      res_data["cover_image_link"] = "";
      res_data["title"] = "Archived";
      res_data["tags"] = [];
      res_data["content"] = "This post has been archived by the user.";
    }

    res.json({ sucess: true, data: res_data });
  }
);

post_router.post(
  "/:postID/like",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let postID: string = req.params?.postID;

    if (req.userData === undefined) {
      res.json({ success: false, msg: "user not logged in" });
      return;
    }

    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
      session.startTransaction();

      let db = mongo_client.db(process.env.DBNAME);

      // check if user has already liked  the post
      let user_data: any = await db
        .collection("users")
        .findOne({ uid: req.userData.uid });

      if (Object.keys(user_data).indexOf("liked_posts") == -1) {
        user_data["liked_posts"] = [];
      }

      if (user_data?.liked_posts.indexOf(postID) != -1) {
        error_msg = "Already liked";
        throw new Error("already liked");
      }

      // update like count
      await db
        .collection("posts")
        .updateOne({ _id: new ObjectId(postID) }, { $inc: { like_count: 1 } });

      // add postID to user doc
      await db
        .collection("users")
        .updateOne(
          { uid: req.userData.uid },
          { $push: { liked_posts: postID } }
        );

      await session.commitTransaction();
      console.log("Transaction committed.");
    } catch (error) {
      await session.abortTransaction();

      console.log(error);
      res.status(500);
      res.json({ success: false, msg: error_msg });
      return;
    } finally {
      await session.endSession();
    }

    res.json({ sucess: true });
  }
);

post_router.post(
  "/:postID/unlike",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let postID: string = req.params?.postID;

    if (req.userData === undefined) {
      res.json({ success: false, msg: "user not logged in" });
      return;
    }

    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
      session.startTransaction();

      let db = mongo_client.db(process.env.DBNAME);

      // check if user has already liked  the post
      let user_data: any = await db
        .collection("users")
        .findOne({ uid: req.userData.uid });

      if (Object.keys(user_data).indexOf("liked_posts") == -1) {
        user_data["liked_posts"] = [];
      }

      if (user_data?.liked_posts.indexOf(postID) == -1) {
        error_msg = "not liked yet";
        throw new Error("already liked yet");
      }

      // update like count
      await db
        .collection("posts")
        .updateOne({ _id: new ObjectId(postID) }, { $inc: { like_count: -1 } });

      // add postID to user doc
      await db
        .collection("users")
        .updateOne(
          { uid: req.userData.uid },
          { $pull: { liked_posts: postID } }
        );

      await session.commitTransaction();
      console.log("Transaction committed.");
    } catch (error) {
      await session.abortTransaction();

      console.log(error);
      res.status(500);
      res.json({ success: false, msg: error_msg });
      return;
    } finally {
      await session.endSession();
    }

    res.json({ sucess: true });
  }
);

post_router.post(
  "/:postID/add-to-bookmark",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let postID: string = req.params?.postID;

    if (req.userData === undefined) {
      res.json({ success: false, msg: "user not logged in" });
      return;
    }

    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
      session.startTransaction();

      let db = mongo_client.db(process.env.DBNAME);

      // check if user has already liked  the post
      let user_data: any = await db
        .collection("users")
        .findOne({ uid: req.userData.uid });

      if (Object.keys(user_data).indexOf("bookmarked_posts") == -1) {
        user_data["bookmarked_posts"] = [];
      }

      if (user_data?.bookmarked_posts.indexOf(postID) != -1) {
        error_msg = "Already bookmarked";
        throw new Error("Already bookmarked");
      }

      // add postID to user bookmark doc
      await db
        .collection("users")
        .updateOne(
          { uid: req.userData.uid },
          { $push: { bookmarked_posts: postID } }
        );

      await session.commitTransaction();
      console.log("Transaction committed.");
    } catch (error) {
      await session.abortTransaction();

      console.log(error);
      res.status(500);
      res.json({ success: false, msg: error_msg });
      return;
    } finally {
      await session.endSession();
    }

    res.json({ sucess: true });
  }
);

post_router.post(
  "/:postID/remove-from-bookmark",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let postID: string = req.params?.postID;

    if (req.userData === undefined) {
      res.json({ success: false, msg: "user not logged in" });
      return;
    }

    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
      session.startTransaction();

      let db = mongo_client.db(process.env.DBNAME);

      // check if user has already liked  the post
      let user_data: any = await db
        .collection("users")
        .findOne({ uid: req.userData.uid });

      if (Object.keys(user_data).indexOf("bookmarked_posts") == -1) {
        user_data["bookmarked_posts"] = [];
      }

      if (user_data?.bookmarked_posts.indexOf(postID) == -1) {
        error_msg = "Not in bookmark yet";
        throw new Error("Not in bookmark yet");
      }

      // add postID to user bookmark doc
      await db
        .collection("users")
        .updateOne(
          { uid: req.userData.uid },
          { $pull: { bookmarked_posts: postID } }
        );

      await session.commitTransaction();
      console.log("Transaction committed.");
    } catch (error) {
      await session.abortTransaction();

      console.log(error);
      res.status(500);
      res.json({ success: false, msg: error_msg });
      return;
    } finally {
      await session.endSession();
    }

    res.json({ sucess: true });
  }
);

post_router.post(
  "/:postID/add-to-archive",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let postID: string = req.params?.postID;

    if (req.userData === undefined) {
      res.json({ success: false, msg: "user not logged in" });
      return;
    }

    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
      session.startTransaction();

      let db = mongo_client.db(process.env.DBNAME);

      // check if user has already liked  the post
      let post_data: any = await db
        .collection("posts")
        .findOne({ _id: new ObjectId(postID) });

      if (post_data["author_uid"] != req.userData.uid) {
        error_msg = "Unauthorised access";
        throw new Error("Unauthorised access");
      }

      if (post_data?.is_archived) {
        error_msg = "Already archived";
        throw new Error("Already archived");
      }

      // add postID to user bookmark doc
      await db
        .collection("posts")
        .updateOne(
          { _id: new ObjectId(postID) },
          { $set: { is_archived: true } }
        );

      await session.commitTransaction();
      console.log("Transaction committed.");
    } catch (error) {
      await session.abortTransaction();

      console.log(error);
      res.status(500);
      res.json({ success: false, msg: error_msg });
      return;
    } finally {
      await session.endSession();
    }

    res.json({ sucess: true });
  }
);

post_router.post(
  "/:postID/remove-from-archive",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let postID: string = req.params?.postID;

    if (req.userData === undefined) {
      res.json({ success: false, msg: "user not logged in" });
      return;
    }

    const session = mongo_client.startSession();
    let error_msg = "server error";
    try {
      session.startTransaction();

      let db = mongo_client.db(process.env.DBNAME);

      // check if user has already liked  the post
      let post_data: any = await db
        .collection("posts")
        .findOne({ _id: new ObjectId(postID) });

      if (post_data["author_uid"] != req.userData.uid) {
        error_msg = "Unauthorised access";
        throw new Error("Unauthorised access");
      }

      if (post_data?.is_archived == false) {
        error_msg = "Already not in archived";
        throw new Error("Already not in archived");
      }

      // add postID to user bookmark doc
      await db
        .collection("posts")
        .updateOne(
          { _id: new ObjectId(postID) },
          { $set: { is_archived: false } }
        );

      await session.commitTransaction();
      console.log("Transaction committed.");
    } catch (error) {
      await session.abortTransaction();

      console.log(error);
      res.status(500);
      res.json({ success: false, msg: error_msg });
      return;
    } finally {
      await session.endSession();
    }

    res.json({ sucess: true });
  }
);

// all comments
post_router.get("/:postID/comments", async (req: any, res: any) => {
  let postID: string = req.params?.postID;

  let comm_res: any = [];
  try {
    let db = mongo_client.db(process.env.DBNAME);

    comm_res = db
      .collection("comments")
      .find({ post_id: postID, parent_comment_id: "" })
      .sort({ modified_at: -1 });
    // console.log(comm_res);
    if (comm_res === null) throw new Error("post not found");
    comm_res = await comm_res.toArray();
  } catch (error) {
    console.log(error);
    res.status(500);
    res.json({ success: true, data: [], msg: "server error" });
    return;
  }

  res.json({ success: true, data: comm_res });
});

export default post_router;
