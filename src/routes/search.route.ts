import express from "express";
import mongo_client from "./../services/db.service.js";

const search_router = express.Router({ mergeParams: true });

search_router.post("/posts", async (req: any, res: any) => {
  let title = req.body?.title;
  // console.log(title);


  if (!title || title.length < 3) {
    res.status(403);
    res.json({ status: false, msg: "invalid input!", data: [] });
    return;
  }

  let error_msg = "server error";
  let posts = []
  try {
    let db = mongo_client.db(process.env.DBNAME);
    const agg = [
      {
        $search: {
          index: 'search_posts',
          text: {
            query: title,
            path: {
              wildcard: "*"
            },
            fuzzy: {
              maxEdits: 2,
              prefixLength: 0,
              maxExpansions: 50
            }
          }
        }
      },
      { $sort: { modified_at: -1 } },
      { $limit: 20 },
    ];
    // run pipeline
    let result = db.collection("posts").aggregate(agg);
    posts = await result.toArray();

    // console.log(posts);
  } catch (error) {
    console.log(error);
    res.status(500);
    res.json({ success: false, msg: error_msg });
    return;
  }

  res.json({ sucess: true, data: posts });
});

search_router.post("/autocomplete", async (req: any, res: any) => {
  let title = req.body?.title;

  if (!title || title.length <= 3) {
    res.status(403);
    res.json({ status: false, msg: "invalid input!" });
    return;
  }

  let error_msg = "server error";

  try {
    let db = mongo_client.db(process.env.DBNAME);
    const agg = [
      {
        $search: {
          index: "search_posts",
          autocomplete: { query: title, path: "title" },
        },
      },
      { $limit: 20 },
      { $project: { _id: 0, title: 1 } },
    ];
    // run pipeline
    let result = db.collection("posts").aggregate(agg);
    let posts = await result.toArray();

    console.log(posts);
  } catch (error) {
    console.log(error);
    res.status(500);
    res.json({ success: false, msg: error_msg });
    return;
  }

  res.json({ sucess: true });
});

export default search_router;
