import express from "express";
import mongo_client from "./../services/db.service.js";
import { ObjectId } from "mongodb";
import path from "path";

// middleware
import {
  check403,
  fetchLoggedInUser,
} from "./../middleware/auth.middleware.js";

const __dirname = path.resolve();
// console.log(__dirname);

const user_router = express.Router({ mergeParams: true });

user_router.use("/", express.static(__dirname + "/public"));

user_router.get("/", (req: any, res: any) => {
  res.redirect("/");
});

user_router.get("/:userUID", async (req: any, res: any) => {
  let userUID: string = req.params?.userUID;

  try {
    let db = mongo_client.db(process.env.DBNAME);

    let res = await db.collection("users").findOne({ uid: userUID });
    // console.log(res);
    if (res === null) throw new Error("user not found");
  } catch (error) {
    console.log(error);
    res.status(404);
    res.sendFile("404.html", { root: "./public" });
    return;
  }

  res.sendFile("profile-page.html", { root: "./public" });
});

user_router.get("/:userUID/data", async (req: any, res: any) => {
  let userUID: string = req.params?.userUID;

  let res_data: any;
  try {
    let db = mongo_client.db(process.env.DBNAME);

    res_data = await db.collection("users").findOne({ uid: userUID });
    // console.log(res);
  } catch (error) {
    console.log(error);
    res.status(404);
    res.json({ success: false });
    return;
  }

  res.json({ sucess: true, data: res_data });
});

user_router.get("/:userUID/all-posts", async (req: any, res: any) => {
  let userUID: string = req.params?.userUID;

  let res_data: any;
  try {
    let db = mongo_client.db(process.env.DBNAME);

    res_data = db.collection("posts").find({ author_uid: userUID });
    res_data = await res_data.toArray();
    // console.log(res);
  } catch (error) {
    console.log(error);
    res.status(404);
    res.json({ success: false });
    return;
  }

  res.json({ sucess: true, data: res_data });
});

user_router.get("/:userUID/all-comments", async (req: any, res: any) => {
  let userUID: string = req.params?.userUID;

  let res_data: any;
  try {
    let db = mongo_client.db(process.env.DBNAME);

    res_data = db.collection("comments").find({ author_uid: userUID });
    res_data = await res_data.toArray();
    // console.log(res);
  } catch (error) {
    console.log(error);
    res.status(404);
    res.json({ success: false });
    return;
  }

  res.json({ sucess: true, data: res_data });
});

user_router.get(
  "/:userUID/liked-posts",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let userUID: string = req.params?.userUID;

    if (req.userData === undefined || userUID != req.userData.uid) {
      res.status(403);
      res.json({
        success: false,
        data: [],
        msg: "unauthorised access requested !",
      });
      return;
    }

    let res_data: any;
    try {
      let db = mongo_client.db(process.env.DBNAME);

      let user_data: any = await db
        .collection("users")
        .findOne({ uid: userUID });

      if (Object.keys(user_data).indexOf("liked_posts") == -1) {
        res.json({ success: true, data: [] });
        return;
      }
      let ids = user_data["liked_posts"];

      ids = ids.map((e: any) => {
        return new ObjectId(e);
      });

      res_data = db.collection("posts").find({ _id: { $in: ids } });
      res_data = await res_data.toArray();
      // console.log(res);
    } catch (error) {
      console.log(error);
      res.status(404);
      res.json({ success: false });
      return;
    }

    res.json({ sucess: true, data: res_data });
  }
);

user_router.get(
  "/:userUID/liked-comments",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let userUID: string = req.params?.userUID;

    if (req.userData === undefined || userUID != req.userData.uid) {
      res.status(403);
      res.json({
        success: false,
        data: [],
        msg: "unauthorised access requested !",
      });
      return;
    }

    let res_data: any;
    try {
      let db = mongo_client.db(process.env.DBNAME);

      let user_data: any = await db
        .collection("users")
        .findOne({ uid: userUID });

      if (Object.keys(user_data).indexOf("liked_comments") == -1) {
        res.json({ success: true, data: [] });
        return;
      }
      let ids = user_data["liked_comments"];

      ids = ids.map((e: any) => {
        return new ObjectId(e);
      });

      res_data = db.collection("comments").find({ _id: { $in: ids } });
      res_data = await res_data.toArray();
      // console.log(res);
    } catch (error) {
      console.log(error);
      res.status(404);
      res.json({ success: false });
      return;
    }

    res.json({ sucess: true, data: res_data });
  }
);

user_router.get(
  "/:userUID/archived-posts",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let userUID: string = req.params?.userUID;

    if (req.userData === undefined || userUID != req.userData.uid) {
      res.status(403);
      res.json({
        success: false,
        data: [],
        msg: "unauthorised access requested !",
      });
      return;
    }

    let res_data: any;
    try {
      let db = mongo_client.db(process.env.DBNAME);

      let user_data: any = await db
        .collection("users")
        .findOne({ uid: userUID });

      res_data = db
        .collection("posts")
        .find({ author_uid: req.userData.uid, is_archived: true });
      res_data = await res_data.toArray();
      // console.log(res);
    } catch (error) {
      console.log(error);
      res.status(404);
      res.json({ success: false });
      return;
    }

    res.json({ sucess: true, data: res_data });
  }
);

user_router.get(
  "/:userUID/bookmarked-posts",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let userUID: string = req.params?.userUID;

    if (req.userData === undefined || userUID != req.userData.uid) {
      res.status(403);
      res.json({
        success: false,
        data: [],
        msg: "unauthorised access requested !",
      });
      return;
    }

    let res_data: any;
    try {
      let db = mongo_client.db(process.env.DBNAME);

      let user_data: any = await db
        .collection("users")
        .findOne({ uid: userUID });

      if (Object.keys(user_data).indexOf("bookmarked_posts") == -1) {
        res.json({ success: true, data: [] });
        return;
      }
      let ids = user_data["bookmarked_posts"];

      ids = ids.map((e: any) => {
        return new ObjectId(e);
      });

      res_data = db.collection("posts").find({ _id: { $in: ids } });
      res_data = await res_data.toArray();
      // console.log(res);
    } catch (error) {
      console.log(error);
      res.status(404);
      res.json({ success: false });
      return;
    }

    res.json({ sucess: true, data: res_data });
  }
);

user_router.get(
  "/:userUID/uploads",
  fetchLoggedInUser,
  async (req: any, res: any) => {
    let userUID: string = req.params?.userUID;

    if (req.userData === undefined || userUID != req.userData.uid) {
      res.status(403);
      res.json({
        success: false,
        data: [],
        msg: "unauthorised access requested !",
      });
      return;
    }

    let data: any = [];
    try {
      let db = mongo_client.db(process.env.DBNAME);

      data = db
        .collection("uploads")
        .find({ user_email: req.userData.email })
        .sort({ created_at: -1 });
      data = await data.toArray();
    } catch (e) {
      console.log(e);
      res.status(500);
      res.json({ success: false, data });
      return;
    }
    res.json({ success: true, data });
  }
);

// all comments
user_router.get("/:userUID/comments", async (req: any, res: any) => {
  let userUID: string = req.params?.userUID;

  let comm_res: any = [];
  try {
    let db = mongo_client.db(process.env.DBNAME);

    comm_res = db
      .collection("comments")
      .find({ author_uid: userUID })
      .sort({ modified_at: -1 });
    // console.log(comm_res);
    if (comm_res === null) throw new Error("post not found");
    comm_res = await comm_res.toArray();
  } catch (error) {
    console.log(error);
    res.status(500);
    res.json({ success: true, data: [], msg: "server error" });
    return;
  }

  res.json({ success: true, data: comm_res });
});

export default user_router;
