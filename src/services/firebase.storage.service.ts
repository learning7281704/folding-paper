import admin from "firebase-admin";

const bucket = admin.storage().bucket();

export const uploadImage = async (
  destination: string,
  image: Buffer,
  file_extension: string
) => {
  const file = bucket.file(destination);
  await file.save(image, { contentType: file_extension });

  let file_name = `https://firebasestorage.googleapis.com/v0/b/folding-paper.appspot.com/o/${destination}?alt=media`;
  return file_name;
};
