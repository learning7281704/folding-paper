import Joi from "joi";

const post_schema = Joi.object().keys({
  _id: Joi.string().min(3),
  title: Joi.string().required().min(3).max(100),
  cover_image_link: Joi.string().uri().required().min(3).max(500),
  tags: Joi.array().items(Joi.string().min(3).max(50)).required(),
  content: Joi.string().required().min(3).max(10000),
});

export default post_schema;
